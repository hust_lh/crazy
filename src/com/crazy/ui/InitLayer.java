package com.crazy.ui;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCMultiplexLayer;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItem;
import org.cocos2d.menus.CCMenuItemImage;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;

public class InitLayer extends CCLayer {
	//initLayer sprite
//	private CCSprite initBg;
//	private CCSprite startBtn;
//	private CCSprite modelBtn;
//	private CCSprite groupBtn;
//	private CCSprite helpBtn;
//	private CCSprite settingBtn;
	
//	private boolean flag = false;
	
	public InitLayer(){
		layoutInit();
	}
	public void layoutInit(){
		this.setIsTouchEnabled(true);//���õ�ǰͼ���Ƿ���մ����¼�
		//Ϊ��ʼ�������ñ���
		CCSprite initBg = CCSprite.sprite("initFolder/init_bg.jpg");//��ʼ���������
		CGPoint bgPoint = CGPoint.ccp((0+Common.getSpriteWidth(initBg)),(0+Common.getSpriteHeight(initBg)));//���þ�������λ��
		initBg.setPosition(bgPoint);
		this.addChild(initBg);//����������������������
		
		//startBtn sprite
		CCSprite startBtn = CCSprite.sprite("initFolder/start.png");
		CCMenuItem startItem = CCMenuItemImage.item("initFolder/start.png", "initFolder/start_1.png",this,"startJumpTo");
		CCMenu startMenu = CCMenu.menu(startItem);
		CGPoint startPoint = CGPoint.ccp(
				1098+Common.getSpriteWidth(startBtn),
				(Common.WIN_HEIGHT-580)+Common.getSpriteHeight(startBtn));
		startMenu.setPosition(startPoint);
		this.addChild(startMenu);
//		CCIntervalAction sc = CCScaleBy.action(1.0f, 1.1f);//action(����Ƶ�ʣ����ű���)
//		CCIntervalAction sc_back = sc.reverse();
//		startBtn.runAction(CCRepeatForever.action(CCSequence.actions(sc, sc_back)));
				
		//modelBtn sprite
		CCSprite modelBtn = CCSprite.sprite("initFolder/model.png");
		CCMenuItem modeItem = CCMenuItemImage.item("initFolder/model.png", "initFolder/model_1.png",this,"modelJumpTo");
		CCMenu modelMenu = CCMenu.menu(modeItem);
		CGPoint modelPoint = CGPoint.ccp(
				1078+Common.getSpriteWidth(modelBtn),
				(Common.WIN_HEIGHT-791)+Common.getSpriteHeight(modelBtn));
		modelMenu.setPosition(modelPoint);
		this.addChild(modelMenu);
				
		//groupBtn sprite
		CCSprite groupBtn = CCSprite.sprite("initFolder/group.png");
		CCMenuItem groupItem = CCMenuItemImage.item("initFolder/group.png", "initFolder/group_1.png",this,"groupJumpTo");
		CCMenu groupMenu = CCMenu.menu(groupItem);
		CGPoint groupPoint = CGPoint.ccp(
				1042+Common.getSpriteWidth(groupBtn),
				(Common.WIN_HEIGHT-1014)+Common.getSpriteHeight(groupBtn));
		groupMenu.setPosition(groupPoint);
		this.addChild(groupMenu);

		//helpBtn sprite
		CCSprite helpBtn = CCSprite.sprite("initFolder/help.png");
		CCMenuItem helpItem = CCMenuItemImage.item("initFolder/help.png", "initFolder/help_1.png",this,"helpJumpTo");
		CCMenu helpMenu = CCMenu.menu(helpItem);
		CGPoint helpPoint = CGPoint.ccp(
				1272+Common.getSpriteWidth(helpBtn),
				(Common.WIN_HEIGHT-1014)+Common.getSpriteHeight(helpBtn));
		helpMenu.setPosition(helpPoint);
		this.addChild(helpMenu);

		//settingBtn sprite
		CCSprite settingBtn = CCSprite.sprite("initFolder/setting.png");
		CCMenuItem settingItem = CCMenuItemImage.item("initFolder/setting.png", "initFolder/setting_1.png",this,"settingJumpTo");
		CCMenu settingMenu = CCMenu.menu(settingItem);
		CGPoint settingPoint = CGPoint.ccp(
				1502+Common.getSpriteWidth(settingBtn),
				(Common.WIN_HEIGHT-1014)+Common.getSpriteHeight(settingBtn));
		settingMenu.setPosition(settingPoint);
		this.addChild(settingMenu);
		
	}
	
	public void startJumpTo(Object sender){
		((CCMultiplexLayer) getParent()).switchTo(1);
		Common.flag = 0;
	}
	
	public void modelJumpTo(Object sender){
		((CCMultiplexLayer) getParent()).switchTo(2);
	}
	
	public void groupJumpTo(Object sender){
		((CCMultiplexLayer) getParent()).switchTo(3);
	}
	
	public void helpJumpTo(Object sender){
		((CCMultiplexLayer) getParent()).switchTo(4);
	}
	
	public void settingJumpTo(Object sender){
		((CCMultiplexLayer) getParent()).switchTo(5);
	}
	
}