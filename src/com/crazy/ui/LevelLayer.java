package com.crazy.ui;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCMultiplexLayer;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import android.view.MotionEvent;

/****LevelLayer class******/
public class LevelLayer extends CCLayer{
	//levelLayer sprite
	private CCSprite levelBg;
	private CCSprite title;
	private CCSprite level1Btn;
	private CCSprite level2Btn;
	private CCSprite level3Btn;
	private CCSprite level4Btn;
	private CCSprite level5Btn;
	private CCSprite path;
	private CCSprite backBtn;
	
	private CGPoint cgPoint;
	private Common common = new Common();
	
	public LevelLayer(){
		this.setIsTouchEnabled(true);
		layoutLevel();
	}
	
	public void layoutLevel(){
		levelBg = CCSprite.sprite("levelFolder/level_bg.jpg");
		CGPoint levelBgPoint = CGPoint.ccp(0+Common.getSpriteWidth(levelBg), 0+Common.getSpriteHeight(levelBg));
		levelBg.setPosition(levelBgPoint);
		this.addChild(levelBg);
		
		title = CCSprite.sprite("levelFolder/title.png");
		CGPoint titlePoint = CGPoint.ccp(
				642+Common.getSpriteWidth(title), 
				(Common.WIN_HEIGHT-300)+Common.getSpriteHeight(title));
		title.setPosition(titlePoint);
		this.addChild(title);
		
		path = CCSprite.sprite("levelFolder/path.png");
		CGPoint pathPoint = CGPoint.ccp(
				294+Common.getSpriteWidth(path), 
				(Common.WIN_HEIGHT-794)+Common.getSpriteHeight(path));
		path.setPosition(pathPoint);
		this.addChild(path);
		
		level1Btn = CCSprite.sprite("levelFolder/level_1.png");
		CGPoint level1Point = CGPoint.ccp(
				120+Common.getSpriteWidth(level1Btn), 
				(Common.WIN_HEIGHT-994)+Common.getSpriteHeight(level1Btn));
		level1Btn.setPosition(level1Point);
		this.addChild(level1Btn);
		
		level2Btn = CCSprite.sprite("levelFolder/level_2_1.png");
		CGPoint level2Point = CGPoint.ccp(
				378+Common.getSpriteWidth(level2Btn), 
				(Common.WIN_HEIGHT-622)+Common.getSpriteHeight(level2Btn));
		level2Btn.setPosition(level2Point);
		this.addChild(level2Btn);
		
		level3Btn = CCSprite.sprite("levelFolder/level_3_1.png");
		CGPoint level3Point = CGPoint.ccp(
				710+Common.getSpriteWidth(level3Btn), 
				(Common.WIN_HEIGHT-938)+Common.getSpriteHeight(level3Btn));
		level3Btn.setPosition(level3Point);
		this.addChild(level3Btn);
		
		level4Btn = CCSprite.sprite("levelFolder/level_4_1.png");
		CGPoint level4Point = CGPoint.ccp(
				1038+Common.getSpriteWidth(level4Btn), 
				(Common.WIN_HEIGHT-628)+Common.getSpriteHeight(level4Btn));
		level4Btn.setPosition(level4Point);
		this.addChild(level4Btn);
		
		level5Btn = CCSprite.sprite("levelFolder/level_5_1.png");
		CGPoint level5Point = CGPoint.ccp(
				1328+Common.getSpriteWidth(level5Btn), 
				(Common.WIN_HEIGHT-990)+Common.getSpriteHeight(level5Btn));
		level5Btn.setPosition(level5Point);
		this.addChild(level5Btn);
		
		backBtn = CCSprite.sprite("levelFolder/backBtn.png");
		CGPoint backPoint = CGPoint.ccp(
				168+Common.getSpriteWidth(backBtn), 
				(Common.WIN_HEIGHT-222)+Common.getSpriteHeight(backBtn));
		backBtn.setPosition(backPoint);
		this.addChild(backBtn);
		
//		backBtn_press = CCSprite.sprite("levelFolder/backBtn_1.png");
//		CGPoint backPressPoint = CGPoint.ccp(
//				168+GameLayer.this.getSpriteWidth(backBtn_press), 
//				(WIN_HEIGHT-222)+GameLayer.this.getSpriteHeight(backBtn_press));
//		backBtn_press.setPosition(backPressPoint);
//		backBtn_press.runAction(hide);
//		this.addChild(backBtn_press);
	}

	@Override		
	public boolean ccTouchesEnded(MotionEvent event) {
		// TODO Auto-generated method stub
		cgPoint = common.getTouchPoint(event);
		if(CGRect.containsPoint(backBtn.getBoundingBox(), cgPoint) && Common.flag == 0){
			((CCMultiplexLayer) getParent()).switchTo(0);
		}
		if(CGRect.containsPoint(backBtn.getBoundingBox(), cgPoint) && (Common.flag == 1)){
			((CCMultiplexLayer) getParent()).switchTo(2);//��ת��modelѡ�����
		}
		if(CGRect.containsPoint(level1Btn.getBoundingBox(), cgPoint)){
			((CCMultiplexLayer) getParent()).switchTo(10);//��ת����Ϸ����
			Common.levelMusicFlag = 1;
		}
		return super.ccTouchesBegan(event);
	}		
}
