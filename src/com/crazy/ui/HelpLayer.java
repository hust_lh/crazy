package com.crazy.ui;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCMultiplexLayer;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItem;
import org.cocos2d.menus.CCMenuItemImage;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import android.view.MotionEvent;

public class HelpLayer extends CCLayer{
	
	private CCSprite helpBg;
	private CCSprite backBtn;
	
	private CGPoint cgPoint;
	private Common common = new Common();
	
	public HelpLayer(){
		this.setIsTouchEnabled(true);
		helpLayer();
	}
	
	public void helpLayer(){
		helpBg = CCSprite.sprite("help/help.jpg");
		CGPoint helpPoint = CGPoint.ccp(0+Common.getSpriteWidth(helpBg), 0+Common.getSpriteHeight(helpBg));
		helpBg.setPosition(helpPoint);
		this.addChild(helpBg);

		backBtn = CCSprite.sprite("help/backBtn.png");
		CCMenuItem backItem = CCMenuItemImage.item("help/backBtn.png", "help/backBtn_1.png",this,"helpJumpTo");
		CCMenu backMenu = CCMenu.menu(backItem);
		CGPoint backPoint = CGPoint.ccp(
				104+Common.getSpriteWidth(backBtn), 
				(Common.WIN_HEIGHT-150)+Common.getSpriteHeight(backBtn));
		backMenu.setPosition(backPoint);
		this.addChild(backMenu);
	}
	
	public void helpJumpTo(Object sender){
		((CCMultiplexLayer) getParent()).switchTo(0); 
	}


	@Override
	public boolean ccTouchesEnded(MotionEvent event) {
		// TODO Auto-generated method stub
		cgPoint = common.getTouchPoint(event);
		if(CGRect.containsPoint(backBtn.getBoundingBox(), cgPoint)){
			((CCMultiplexLayer) getParent()).switchTo(0); 
		}
		return super.ccTouchesEnded(event);
	}
}


