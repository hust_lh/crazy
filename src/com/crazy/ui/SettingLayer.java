package com.crazy.ui;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCMultiplexLayer;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItem;
import org.cocos2d.menus.CCMenuItemImage;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import android.view.MotionEvent;

public class SettingLayer extends CCLayer{
	private CCSprite settingBg;
	private CCSprite titile;
	private CCSprite music,music_press;
	private CCSprite sound,sound_press;
	private CCSprite control,control_press;
	private CCSprite userInfo;
	private CCSprite myRank;
	private CCSprite backBtn;
	
	private CGPoint cgPoint;

	public static boolean soundFlag = false;
	public static boolean musicFlag = false;
	private boolean controlFlag = false;
	
	private Common common = new Common();
	
	public SettingLayer(){
		this.setIsTouchEnabled(true);
		settingLayer();
	}
	
	public void settingLayer(){
		settingBg = CCSprite.sprite("setting/setting_bg.jpg");
		CGPoint settingPoint = CGPoint.ccp(0+Common.getSpriteWidth(settingBg), 0+Common.getSpriteHeight(settingBg));
		settingBg.setPosition(settingPoint);
		this.addChild(settingBg);
		
		titile = CCSprite.sprite("setting/title.png");
		CGPoint titilePoint = CGPoint.ccp(
				712+Common.getSpriteWidth(titile), 
				(Common.WIN_HEIGHT-286)+Common.getSpriteHeight(titile));
		titile.setPosition(titilePoint);
		this.addChild(titile);
		
		userInfo = CCSprite.sprite("setting/user_1.png");
		CCMenuItem userItem = CCMenuItemImage.item("setting/user_1.png", "setting/user_2.png",this,"userJumpTo");
		CCMenu userMenu = CCMenu.menu(userItem);
//		userMenu.alignItemsVertically();
		CGPoint userItemPoint = CGPoint.ccp(
				550+Common.getSpriteWidth(userInfo), 
				(Common.WIN_HEIGHT-650)+Common.getSpriteHeight(userInfo));
		userMenu.setPosition(userItemPoint);
		settingBg.addChild(userMenu);
		
		myRank = CCSprite.sprite("setting/rank_1.png");
		CCMenuItem rankItem = CCMenuItemImage.item("setting/rank_1.png", "setting/rank_2.png",this,"rankJumpTo");
		CCMenu rankMenu = CCMenu.menu(rankItem);
		CGPoint rankItemPoint = CGPoint.ccp(
				1075+Common.getSpriteWidth(myRank), 
				(Common.WIN_HEIGHT-650)+Common.getSpriteHeight(myRank));
		rankMenu.setPosition(rankItemPoint);
		this.addChild(rankMenu);
		
		sound = CCSprite.sprite("setting/sound_on.png");
		CGPoint soundPoint = CGPoint.ccp(
				296+Common.getSpriteWidth(sound), 
				(Common.WIN_HEIGHT-915)+Common.getSpriteHeight(sound));
		sound.setPosition(soundPoint);
		this.addChild(sound);
		
		sound_press = CCSprite.sprite("setting/sound_off.png");
		CGPoint soundPressPoint = CGPoint.ccp(
				296+Common.getSpriteWidth(sound_press), 
				(Common.WIN_HEIGHT-915)+Common.getSpriteHeight(sound_press));
		sound_press.setPosition(soundPressPoint);
		sound_press.runAction(Common.hide);
		this.addChild(sound_press);
		
		music = CCSprite.sprite("setting/music_on.png");
		CGPoint musicPoint = CGPoint.ccp(
				805+Common.getSpriteWidth(music), 
				(Common.WIN_HEIGHT-915)+Common.getSpriteHeight(music));
		music.setPosition(musicPoint);
		this.addChild(music);
		
		music_press = CCSprite.sprite("setting/music_off.png");
		CGPoint musicPressPoint = CGPoint.ccp(
				805+Common.getSpriteWidth(music_press), 
				(Common.WIN_HEIGHT-915)+Common.getSpriteHeight(music_press));
		music_press.setPosition(musicPressPoint);
		music_press.runAction(Common.hide);
		this.addChild(music_press);
		
		control = CCSprite.sprite("setting/touch_on.png");
		CGPoint controlPoint = CGPoint.ccp(
				1310+Common.getSpriteWidth(control), 
				(Common.WIN_HEIGHT-915)+Common.getSpriteHeight(control));
		control.setPosition(controlPoint);
		this.addChild(control);
		
		control_press = CCSprite.sprite("setting/touch_off.png");
		CGPoint controlPressPoint = CGPoint.ccp(
				1310+Common.getSpriteWidth(control_press), 
				(Common.WIN_HEIGHT-915)+Common.getSpriteHeight(control_press));
		control_press.setPosition(controlPressPoint);
		control_press.runAction(Common.hide);
		this.addChild(control_press);

		backBtn = CCSprite.sprite("setting/backBtn.png");
		CCMenuItem backItem = CCMenuItemImage.item("setting/backBtn.png", "setting/backBtn_1.png",this,"backJumpTo");
		CCMenu backMenu = CCMenu.menu(backItem);
		CGPoint backPoint = CGPoint.ccp(
				104+Common.getSpriteWidth(backBtn), 
				(Common.WIN_HEIGHT-150)+Common.getSpriteHeight(backBtn));
		backMenu.setPosition(backPoint);
		this.addChild(backMenu);
	}
	
	public void backJumpTo(Object sender){
		((CCMultiplexLayer) getParent()).switchTo(0); 
	}
	
	public void userJumpTo(Object sender){
		((CCMultiplexLayer) getParent()).switchTo(8); 
	}
	
	public void rankJumpTo(Object sender){
		((CCMultiplexLayer) getParent()).switchTo(9); 
	}

	@Override
	public boolean ccTouchesEnded(MotionEvent event) {
		// TODO Auto-generated method stub
		if(event.getPointerCount() == 1){
			cgPoint = common.getTouchPoint(event);
			if(CGRect.containsPoint(backBtn.getBoundingBox(), cgPoint)){
				((CCMultiplexLayer) getParent()).switchTo(0); 
			}
			if(common.isClick(cgPoint, music)){
				if(!musicFlag){
					music.runAction(Common.hide);
					music_press.runAction(Common.show);
					musicFlag = true;
					SoundEngine.sharedEngine().pauseSound();
				}else if(musicFlag){
					music.runAction(Common.show);
					music_press.runAction(Common.hide);
					musicFlag = false;
					SoundEngine.sharedEngine().resumeSound();
				}
			}
			if(common.isClick(cgPoint, sound)){
				if(!soundFlag){
					sound.runAction(Common.hide);
					sound_press.runAction(Common.show);
					soundFlag = true;
				}else if(soundFlag){
					sound.runAction(Common.show);
					sound_press.runAction(Common.hide);
					soundFlag = false;
				}
			}
			if(common.isClick(cgPoint, control)){
				if(!controlFlag){
					control.runAction(Common.hide);
					control_press.runAction(Common.show);
					controlFlag = true;
				}else if(controlFlag){
					control.runAction(Common.show);
					control_press.runAction(Common.hide);
					controlFlag = false;
				}
			}
		}
		return super.ccTouchesEnded(event);
	}
	
}