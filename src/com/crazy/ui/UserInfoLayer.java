package com.crazy.ui;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCMultiplexLayer;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItem;
import org.cocos2d.menus.CCMenuItemImage;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;

public class UserInfoLayer extends CCLayer {

	private CCSprite infoBg;
	private CCSprite backBtn;
	
	public UserInfoLayer() {
		// TODO Auto-generated constructor stub
		this.setIsTouchEnabled(true);
		initInfo();
	}
	
	public void initInfo(){
		infoBg = CCSprite.sprite("userInfo/userInfo.jpg");
		CGPoint infoPoint = CGPoint.ccp((0+Common.getSpriteWidth(infoBg)),(0+Common.getSpriteHeight(infoBg)));
		infoBg.setPosition(infoPoint);
		this.addChild(infoBg);

		backBtn = CCSprite.sprite("userInfo/backBtn.png");
		CCMenuItem backItem = CCMenuItemImage.item("userInfo/backBtn.png", "userInfo/backBtn_1.png",this,"infoJumpTo");
		CCMenu backMenu = CCMenu.menu(backItem);
		CGPoint backPoint = CGPoint.ccp(
				104+Common.getSpriteWidth(backBtn), 
				(Common.WIN_HEIGHT-150)+Common.getSpriteHeight(backBtn));
		backMenu.setPosition(backPoint);
		this.addChild(backMenu);
	}
	

	public void infoJumpTo(Object sender){
		((CCMultiplexLayer) getParent()).switchTo(5); 
	}

}
