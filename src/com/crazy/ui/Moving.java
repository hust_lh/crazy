package com.crazy.ui;

import org.cocos2d.actions.base.CCRepeatForever;
import org.cocos2d.actions.interval.CCAnimate;
import org.cocos2d.actions.interval.CCFadeOut;
import org.cocos2d.actions.interval.CCIntervalAction;
import org.cocos2d.actions.interval.CCMoveBy;
import org.cocos2d.actions.interval.CCMoveTo;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCMultiplexLayer;
import org.cocos2d.nodes.CCAnimation;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteSheet;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

public class Moving extends CCLayer {

	private CCSprite movingBack;
	public Moving() {
		// TODO Auto-generated constructor stub
		initMoving();
	}

	public void initMoving(){
//		CCSpriteSheet moving = CCSpriteSheet.spriteSheet("moving/moving.png");
//		moving.setPosition(0, 0);
//		this.addChild(moving);
		
		CCSprite movingBack = CCSprite.sprite("moving/moving1.jpg");
		movingBack.setPosition(900,540);//设置初始位置
		this.schedule("loginLevel1", 5);
		this.addChild(movingBack);
		
		CCSprite boy_run = CCSprite.sprite("moving/boy_run/0.png");
		CCAnimation animationBoy = CCAnimation.animation("movingBoy");
		for(int i=0;i<5;i++)
			animationBoy.addFrame(String.format("moving/boy_run/%d.png", i));
		CCAnimate actionBoy = CCAnimate.action(0.8f,animationBoy,true);
		CCRepeatForever forverBoy = CCRepeatForever.action(actionBoy);
		boy_run.runAction(forverBoy);
		boy_run.setPosition(1000,300);
		movingBack.addChild(boy_run);
		
		CCSprite dog_run = CCSprite.sprite("moving/dog_run1/0.png");
		CCAnimation animationDog = CCAnimation.animation("movingDog");
		for(int i=0;i<9;i++)
			animationDog.addFrame(String.format("moving/dog_run1/%d.png", i));
		CCAnimate actionDog = CCAnimate.action(0.9f,animationDog,true);
		CCRepeatForever forverDog = CCRepeatForever.action(actionDog);
		dog_run.runAction(forverDog);
		dog_run.setPosition(CGPoint.make(600,300));
		movingBack.addChild(dog_run);
		
	}
	
	public void loginLevel1(float delter){
		((CCMultiplexLayer) getParent()).switchTo(6);
		CCFadeOut out = CCFadeOut.action(2.0f);
		movingBack.runAction(out);
	}
	
}
