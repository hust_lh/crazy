package com.crazy.ui;

import org.cocos2d.actions.instant.CCHide;
import org.cocos2d.actions.instant.CCShow;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import android.view.MotionEvent;

public class Common {
	//屏幕尺寸
	public static final float WIN_HEIGHT = CCDirector.sharedDirector().winSize().width;//1080px
	public static final float WIN_WIDTH = CCDirector.sharedDirector().winSize().height;//1800px

	public static CCShow show = CCShow.action();
	public static CCHide hide = CCHide.action();
	
	public static int flag;//levelLayer页面返回判断，判断返回initLayer还是modelLayer
	public static int levelMusicFlag;//每一关卡的背景音乐标识符

	private static float width,height;
	private float x,y;
	private CGPoint touchPoint;
	
	private boolean checkFlag;//按钮点击事件标识

	public Common() {
		// TODO Auto-generated constructor stub
	}

	//获取精灵width
	public static float getSpriteWidth(CCSprite sprite){
		CGRect rect = sprite.getBoundingBox();
		width = CGRect.width(rect)/2;
		return width;
	}
		
	//获取精灵height
	public static float getSpriteHeight(CCSprite sprite){
		CGRect rect = sprite.getBoundingBox();
		height = CGRect.height(rect)/2;
		return height;
	}
	
	//将android屏幕坐标系转化为cocos2d中的OpenGL坐标系
	public CGPoint getTouchPoint(MotionEvent event){
//		if(event.getPointerCount() == 1){
			touchPoint = CCDirector.sharedDirector().convertToGL(CGPoint.ccp(event.getX(), event.getY()));
//		}
		return touchPoint;
	}
		
	//获取精灵对象的位置坐标
	public float getPositionX(CCSprite sprite){
		x = sprite.getPosition().x;
		return x;
	}
	
	public float getPositionY(CCSprite sprite){
		y = sprite.getPosition().y;
		return y;
	}
	
	public boolean isClick(CGPoint cgPoint, CCSprite sprite){
		checkFlag = cgPoint.x>(getPositionX(sprite)-getSpriteWidth(sprite))
				 && cgPoint.x<(getPositionX(sprite)+getSpriteWidth(sprite))
				 && cgPoint.y>(getPositionY(sprite)-getSpriteHeight(sprite))
				 && cgPoint.y<(getPositionY(sprite)+getSpriteHeight(sprite));
		if(checkFlag){
			return true;
		}
		else{
			return false;
		}
	}

}
