﻿package com.crazy.ui;

import com.crazy.service.GameLayer;
import com.crazy.service.MapEdit;
import com.crazy.ui.R;

import org.cocos2d.events.CCTouchDispatcher;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCMultiplexLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.opengl.CCGLSurfaceView;
import org.cocos2d.sound.SoundEngine;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;

public class InitActivity extends Activity {
	
	//声明成员变量
	private CCGLSurfaceView view;//cocos2d将图形绘制到该view上
	private CCMultiplexLayer layers;
	
	public static InitActivity app;
	//获取CCDirector对象
	CCDirector director = CCDirector.sharedDirector();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		app = this;
		
		view =new CCGLSurfaceView(this);
		setContentView(view);

		//设置游戏相关属性
		director.attachInView(view);//设置当前游戏中所使用的view对象
		director.setLandscape(true);//设置游戏的显示模式
		director.setDisplayFPS(true);//设置是否显示FPS
		director.setAnimationInterval(1.0/60.0);//设置当前游戏渲染一帧所需要的时间
		
		//场景相关设置		
		CCScene scene = CCScene.node();//生成场景对象
		
		//创建各场景层对象
		InitLayer initLayer = new InitLayer();		//0
		LevelLayer levelLayer = new LevelLayer();	//1
		ModelLayer modelLayer = new ModelLayer();	//2
		GroupLayer groupLayer = new GroupLayer();	//3
		HelpLayer helpLayer = new HelpLayer();		//4
		SettingLayer settingLayer = new SettingLayer();//5
		GameLayer gameLayer = new GameLayer();		//6
		MapEdit mapEdit = new MapEdit();			//7
		UserInfoLayer userInfoLayer = new UserInfoLayer();	//8
		RankLayer rankLayer = new RankLayer();		//9
		Moving moving = new Moving();		//10
		
		layers = CCMultiplexLayer.node(initLayer,levelLayer,modelLayer,
				groupLayer,helpLayer,settingLayer,gameLayer,mapEdit,userInfoLayer,rankLayer,moving);
		scene.addChild(layers);//将布景层对象添加到场景中
		scene.addChild(new MainLayer());
		
		director.runWithScene(scene);//运行游戏场景
		
		Common.levelMusicFlag = 0;//初始化音乐标识符
		
		SoundEngine.sharedEngine().preloadEffect(app, R.raw.click);
		SoundEngine.sharedEngine().preloadSound(app, R.raw.start_music);
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
	//	getMenuInflater().inflate(R.menu.init, menu);
		return true;
	}	

//	@Override
//	protected void onStart() {
//		// TODO Auto-generated method stub
//		super.onStart();
//	}
//
//	@Override
//	protected void onPause() {
//		// TODO Auto-generated method stub
//		super.onPause();
//		director.onPause();
////		System.exit(0);
//	}
//
//	@Override
//	protected void onResume() {
//		// TODO Auto-generated method stub
//		super.onResume();
//		director.onResume();
//	}
//	
//	@Override
//	protected void onDestroy() {
//		// TODO Auto-generated method stub
//		super.onDestroy();
//		director.end();
////		finish();
////		System.exit(0);
//	}


	static class MainLayer extends CCLayer {
		
		public MainLayer() {
			super();

			this.setIsTouchEnabled(true);
		}

		@Override
		public boolean ccTouchesBegan(MotionEvent event) {
			if(!SettingLayer.soundFlag){
				SoundEngine.sharedEngine().playEffect(app, R.raw.click);
			}else{
				SoundEngine.sharedEngine().stopEffect(app, R.raw.click);
			}

			return CCTouchDispatcher.kEventHandled;
		}

		@Override
		public void onEnter() {
			super.onEnter();
//			if(Common.levelMusicFlag == 0){
				SoundEngine.sharedEngine().playSound(app, R.raw.start_music, true);
//			}else if(Common.levelMusicFlag == 1){
//				SoundEngine.sharedEngine().realesSound(R.raw.start_music);
//				SoundEngine.sharedEngine().playSound(app, R.raw.level_1, true);
//			}else if(Common.levelMusicFlag == 2){
//				SoundEngine.sharedEngine().playSound(app, R.raw.level_2, true);
//			}else if(Common.levelMusicFlag == 3){
//				SoundEngine.sharedEngine().playSound(app, R.raw.level_3, true);
////				SoundEngine.sharedEngine().realesSound(resId);
//			}
		}
		
		

		@Override
		public void onExit() {
			SoundEngine.sharedEngine().pauseSound();
//			SoundEngine.sharedEngine().realesAllEffects();
			
			super.onExit();
		}
		
		
	}
	
}