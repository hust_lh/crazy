package com.crazy.ui;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCMultiplexLayer;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItem;
import org.cocos2d.menus.CCMenuItemImage;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import android.view.MotionEvent;

public class GroupLayer extends CCLayer{
	
	private CCSprite groupPhoto;
	private CCSprite backBtn;
	
	private CGPoint cgPoint;
	private Common common = new Common();
	
	public GroupLayer(){
		this.setIsTouchEnabled(true);
		groupLayout();
	}
	
	public void groupLayout(){
		groupPhoto = CCSprite.sprite("group/group.jpg");
		CGPoint groupPoint = CGPoint.ccp((0+Common.getSpriteWidth(groupPhoto)),(0+Common.getSpriteHeight(groupPhoto)));
		groupPhoto.setPosition(groupPoint);
		this.addChild(groupPhoto);

		backBtn = CCSprite.sprite("group/backBtn.png");
		CCMenuItem backItem = CCMenuItemImage.item("group/backBtn.png", "group/backBtn_1.png",this,"groupJumpTo");
		CCMenu backMenu = CCMenu.menu(backItem);
		CGPoint backPoint = CGPoint.ccp(
				104+Common.getSpriteWidth(backBtn), 
				(Common.WIN_HEIGHT-150)+Common.getSpriteHeight(backBtn));
		backMenu.setPosition(backPoint);
		this.addChild(backMenu);
	}
	
	public void groupJumpTo(Object sender){
		((CCMultiplexLayer) getParent()).switchTo(0); 
	}

//	@Override
//	public boolean ccTouchesEnded(MotionEvent event) {
//		// TODO Auto-generated method stub
//		cgPoint = common.getTouchPoint(event);
//		if(CGRect.containsPoint(backBtn.getBoundingBox(), cgPoint)){
//			((CCMultiplexLayer) getParent()).switchTo(0); 
//		}
//		return super.ccTouchesEnded(event);
//	}
	
}

