package com.crazy.ui;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCMultiplexLayer;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItem;
import org.cocos2d.menus.CCMenuItemImage;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;

public class RankLayer extends CCLayer {

	private CCSprite rankBg;
	private CCSprite backBtn;
	
	public RankLayer() {
		// TODO Auto-generated constructor stub
		this.setIsTouchEnabled(true);
		initRank();
	}
	
	public void initRank(){
		rankBg = CCSprite.sprite("rank/rank.jpg");
		CGPoint infoPoint = CGPoint.ccp((0+Common.getSpriteWidth(rankBg)),(0+Common.getSpriteHeight(rankBg)));
		rankBg.setPosition(infoPoint);
		this.addChild(rankBg);

		backBtn = CCSprite.sprite("rank/backBtn.png");
		CCMenuItem backItem = CCMenuItemImage.item("rank/backBtn.png", "rank/backBtn_1.png",this,"infoJumpTo");
		CCMenu backMenu = CCMenu.menu(backItem);
		CGPoint backPoint = CGPoint.ccp(
				104+Common.getSpriteWidth(backBtn), 
				(Common.WIN_HEIGHT-150)+Common.getSpriteHeight(backBtn));
		backMenu.setPosition(backPoint);
		this.addChild(backMenu);
	}
	

	public void infoJumpTo(Object sender){
		((CCMultiplexLayer) getParent()).switchTo(5); 
	}

}
