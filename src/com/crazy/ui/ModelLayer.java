package com.crazy.ui;

import org.cocos2d.actions.ease.CCEaseExponentialInOut;
import org.cocos2d.actions.ease.CCEaseInOut;
import org.cocos2d.actions.ease.CCEaseOut;
import org.cocos2d.actions.interval.CCIntervalAction;
import org.cocos2d.actions.interval.CCMoveBy;
import org.cocos2d.actions.interval.CCMoveTo;
import org.cocos2d.actions.interval.CCScaleTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.actions.interval.CCSpawn;
import org.cocos2d.actions.interval.CCTintTo;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCMultiplexLayer;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;
import org.cocos2d.types.ccColor3B;

import android.view.MotionEvent;

public class ModelLayer extends CCLayer{
	
	//modelLayer sprite
	private CCSprite modelBg;
	private CCSprite title;
	private CCSprite singleBtn;
	private CCSprite doubleBtn;
	private CCSprite freeBtn;
	private CCSprite backBtn;
	private CCSprite arrow;
	private CCSprite light;
	
	private CGPoint cgPoint;
	private Common common = new Common();
	private int moveFlag = 2;//��ǰmodel��ǡ�2��˫��ģʽ��1������ģʽ��0������ģʽ��ͬʱ�����ж��Ƿ����ؿ�ѡ��ҳ��
	private boolean loginFlag = true;
	
	//���ƻ����¼�����
	CGPoint startPoint;
	CGPoint endPoint;
	CGPoint delter;
	
	public ModelLayer(){
		this.setIsTouchEnabled(true);
		modelLayout();
	}
	
	public void modelLayout(){
		modelBg = CCSprite.sprite("modelLayer/model_bg.jpg");
		CGPoint modelBgPoint = CGPoint.ccp(0+Common.getSpriteWidth(modelBg), 0+Common.getSpriteHeight(modelBg));
		modelBg.setPosition(modelBgPoint);
		this.addChild(modelBg);
		
		light = CCSprite.sprite("modelLayer/light.png");
		CGPoint lightPoint = CGPoint.ccp(
				600+Common.getSpriteWidth(light),
				(Common.WIN_HEIGHT-888)+Common.getSpriteHeight(light));
		light.setPosition(lightPoint);
		this.addChild(light);
		
		title = CCSprite.sprite("modelLayer/title.png");
		CGPoint titlePoint = CGPoint.ccp(
				474+Common.getSpriteWidth(title),
				(Common.WIN_HEIGHT-242)+Common.getSpriteHeight(title));
		title.setPosition(titlePoint);
		this.addChild(title);
		
		singleBtn = CCSprite.sprite("modelLayer/single_2.png");
		CGPoint singlePoint = CGPoint.ccp(
				142+Common.getSpriteWidth(singleBtn),
				(Common.WIN_HEIGHT-832)+Common.getSpriteHeight(singleBtn));
		singleBtn.setPosition(singlePoint);
		CCIntervalAction singleSc = CCScaleTo.action(0.0f,0.6f);
		CCTintTo tintToDarkSingle = CCTintTo.action(0.4f, ccColor3B.ccc3(51, 51, 51));
		singleBtn.runAction(singleSc);
		singleBtn.runAction(tintToDarkSingle);
		this.addChild(singleBtn);

		doubleBtn = CCSprite.sprite("modelLayer/double_2.png");
		CGPoint doublePoint = CGPoint.ccp(
				684+Common.getSpriteWidth(doubleBtn),
				(Common.WIN_HEIGHT-832)+Common.getSpriteHeight(doubleBtn));
		doubleBtn.setPosition(doublePoint);
		this.addChild(doubleBtn);

		freeBtn = CCSprite.sprite("modelLayer/free_2.png");
		CGPoint freePoint = CGPoint.ccp(
				1306+Common.getSpriteWidth(freeBtn),
				(Common.WIN_HEIGHT-832)+Common.getSpriteHeight(freeBtn));
		freeBtn.setPosition(freePoint);
		CCIntervalAction freeSc = CCScaleTo.action(0.0f,0.6f);
		CCTintTo tintToDarkFree = CCTintTo.action(0.4f, ccColor3B.ccc3(51, 51, 51));
		freeBtn.runAction(freeSc);
		freeBtn.runAction(tintToDarkFree);
		this.addChild(freeBtn);

		backBtn = CCSprite.sprite("modelLayer/backBtn.png");
		CGPoint backPoint = CGPoint.ccp(
				168+Common.getSpriteWidth(backBtn), 
				(Common.WIN_HEIGHT-222)+Common.getSpriteHeight(backBtn));
		backBtn.setPosition(backPoint);
		this.addChild(backBtn);

//		arrow = CCSprite.sprite("modelLayer/arrow.png");
//		CGPoint arrowPoint = CGPoint.ccp(
//				516+Common.getSpriteWidth(arrow), 
//				(Common.WIN_HEIGHT-1004)+Common.getSpriteHeight(arrow));
//		arrow.setPosition(arrowPoint);
//		this.addChild(arrow);
	}
	
	@Override
	public boolean ccTouchesBegan(MotionEvent event) {
		// TODO Auto-generated method stub
		startPoint = common.getTouchPoint(event);
		loginFlag = true;
		return super.ccTouchesBegan(event);
	}
	
	

	@Override
	public boolean ccTouchesEnded(MotionEvent event) {
		// TODO Auto-generated method stub
		/**
		 *model����ѭ��չʾ
		 */
		endPoint = common.getTouchPoint(event);//��ȡ�����¼�����ʱ�����
		delter = CGPoint.ccpSub(startPoint, endPoint);//���㴥���¼������x������
		
		//modelѭ��λ�����
		CGPoint rightPoint = CGPoint.ccp(1530, 472);
		CGPoint middlePoint = CGPoint.ccp(908, 472);
		CGPoint leftPoint = CGPoint.ccp(341, 472);
		/**����Ԥ��**/
		CCMoveTo moveToRight = CCMoveTo.action(0.4f, rightPoint);
		CCIntervalAction moveRight =  CCEaseInOut.action(moveToRight.copy(),2);
	
		CCMoveTo moveToMiddle = CCMoveTo.action(0.4f, middlePoint);
		CCIntervalAction moveMiddle =  CCEaseInOut.action(moveToMiddle.copy(),2);
		
		CCMoveTo moveToLeft = CCMoveTo.action(0.4f, leftPoint);
		CCIntervalAction moveLeft =  CCEaseInOut.action(moveToLeft.copy(),2);
		/**���Ŷ���**/
		CCScaleTo scaleToLarge = CCScaleTo.action(0.4f, 1/0.88f);
		CCScaleTo scaleToSmall = CCScaleTo.action(0.4f, 0.6f);
		
		/**��ɫ�仯**/
		CCTintTo tintToDark = CCTintTo.action(0.4f, ccColor3B.ccc3(81, 81, 81));
		CCTintTo tintToLight = CCTintTo.action(0.4f, ccColor3B.ccc3(255, 255, 255));
		//ѭ������
		switch(moveFlag){
		case 0:
			loginFlag = false;
			if(delter.x > 100){
				CCSpawn spawnMiddle = CCSpawn.actions(moveMiddle, scaleToLarge ,tintToLight);
				singleBtn.runAction(spawnMiddle);
//				singleBtn_1.runAction(spawnMiddle);
				doubleBtn.runAction(moveRight);
//				doubleBtn_1.runAction(moveRight);
				CCSpawn spawnLeft = CCSpawn.actions(moveLeft, scaleToSmall, tintToDark);
				freeBtn.runAction(spawnLeft);
//				freeBtn_1.runAction(moveLeft);
				light.runAction(Common.show);
				moveFlag = 1;
				break;
			}
			if(delter.x < -100){
				singleBtn.runAction(moveLeft);
//				singleBtn_1.runAction(moveLeft);
				CCSpawn spawnMiddle = CCSpawn.actions(moveMiddle, scaleToLarge, tintToLight);
				doubleBtn.runAction(spawnMiddle);
//				doubleBtn_1.runAction(moveMiddle);
				CCSpawn spanRight = CCSpawn.actions(moveRight, scaleToSmall ,tintToDark);
				freeBtn.runAction(spanRight);
//				freeBtn_1.runAction(moveRight);
				light.runAction(Common.show);
				moveFlag = 2;
				break;
			}
		case 1:
			loginFlag = false;
			if(delter.x > 100){
				CCSpawn spanLeft = CCSpawn.actions(moveLeft, scaleToSmall, tintToDark);
				singleBtn.runAction(spanLeft);
//				singleBtn_1.runAction(moveLeft);
				CCSpawn spanMiddle = CCSpawn.actions(moveMiddle, scaleToLarge, tintToLight);
				doubleBtn.runAction(spanMiddle);
//				doubleBtn_1.runAction(moveMiddle);
				freeBtn.runAction(moveRight);
//				freeBtn_1.runAction(moveRight);
				light.runAction(Common.show);
				moveFlag = 2;
				break;
			}
			if(delter.x < -100){
				CCSpawn spanRight = CCSpawn.actions(moveRight, scaleToSmall, tintToDark);
				singleBtn.runAction(spanRight);
//				singleBtn_1.runAction(moveRight);
				doubleBtn.runAction(moveLeft);
//				doubleBtn_1.runAction(moveLeft);
				CCSpawn spanMiddle = CCSpawn.actions(moveMiddle, scaleToLarge ,tintToLight);
				freeBtn.runAction(spanMiddle);
//				freeBtn_1.runAction(moveMiddle);
				light.runAction(Common.show);
				moveFlag = 0;
				break;
			}
		case 2:
			loginFlag = false;
			if(delter.x > 100){
//				light.runAction(Common.hide);
				singleBtn.runAction(moveRight);
//				singleBtn_1.runAction(moveRight);
				CCSpawn spanLeft = CCSpawn.actions(moveLeft, scaleToSmall, tintToDark);
				doubleBtn.runAction(spanLeft);
//				doubleBtn_1.runAction(moveLeft);
				CCSpawn spanMiddle = CCSpawn.actions(moveMiddle, scaleToLarge, tintToLight);
				freeBtn.runAction(spanMiddle);
//				freeBtn_1.runAction(moveMiddle);
////				light.runAction(Common.show);
//				if(common.getPositionX(freeBtn) == 1530){
//					light.runAction(Common.show);
//				}
//				System.out.println("freeBtnX="+common.getPositionX(freeBtn));
				moveFlag = 0;
				break;
			}
			if(delter.x < -100){
				CCSpawn spanMiddle = CCSpawn.actions(moveMiddle, scaleToLarge, tintToLight);
				singleBtn.runAction(spanMiddle);
//				singleBtn_1.runAction(moveMiddle);
				CCSpawn spanRight = CCSpawn.actions(moveRight, scaleToSmall, tintToDark);
				doubleBtn.runAction(spanRight);
//				doubleBtn_1.runAction(moveRight);
				freeBtn.runAction(moveLeft);
//				freeBtn_1.runAction(moveLeft);
				light.runAction(Common.show);
				moveFlag = 1;
				break;
			}
			
			cgPoint = common.getTouchPoint(event);
			if(CGRect.containsPoint(backBtn.getBoundingBox(), cgPoint)){
				((CCMultiplexLayer) getParent()).switchTo(0);
			}
			if(cgPoint.x<1132 && cgPoint.x>684 && cgPoint.y>248 && cgPoint.y<735 && moveFlag == 1){
				((CCMultiplexLayer) getParent()).switchTo(1);
				Common.flag = 1;
//				System.out.println("touchArea:"+doubleBtn.getBoundingBox());
			}
			if(cgPoint.x<1132 && cgPoint.x>684 && cgPoint.y>248 && cgPoint.y<735 && moveFlag == 0){
				((CCMultiplexLayer) getParent()).switchTo(7);
//				System.out.println("touchArea:"+doubleBtn.getBoundingBox());
			}
			
		}
		
		
		
		return super.ccTouchesEnded(event);
	}

	@Override
	public boolean ccTouchesMoved(MotionEvent event) {
		// TODO Auto-generated method stub
//		CGPoint lightEndPoint = CGPoint.ccp(event.getX(), event.getY());
//		CGPoint lightDelterPoint = CGPoint.ccpSub(startPoint, lightEndPoint);
//		if(Math.abs(lightDelterPoint.x)>100){
//			light.runAction(Common.hide);
//		}
		
		return super.ccTouchesMoved(event);
	}
}