package com.crazy.tools;

import org.cocos2d.actions.instant.CCCallFuncND;
import org.cocos2d.actions.interval.CCAnimate;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.nodes.CCAnimation;
import org.cocos2d.nodes.CCSprite;

import com.crazy.service.GameLayer;

public class Effect {

private CCSprite deleteBullet;
private CCSprite deleteBone;
	
	
public Effect(GameLayer gameLayer){
	this.deleteBone=CCSprite.sprite("deleteBone/1.png");
	this.deleteBone.setVisible(false);
	gameLayer.addChild(deleteBone);
	this.deleteBullet=CCSprite.sprite("deleteBaozi/1.png");
	this.deleteBullet.setVisible(false);
	gameLayer.addChild(deleteBullet);
}


public void createDeleteAnimation(){
	CCAnimation animation = CCAnimation.animation("deleteBaozi");
    for( int i=0;i<8;i++)
			animation.addFrame(String.format("deleteBaozi/%d.png",i+1));
   // animation.setDelayPerUnit(0.2f);
	CCAnimate action = CCAnimate.action(1,animation, false);
	//this.deleteBullet.runAction(action);
	CCSequence sequence=CCSequence.actions(action,CCCallFuncND.action(this,"resetVisible"));
	this.deleteBullet.runAction(sequence);
}

public void createDeleteBoneAnimation(){
	CCAnimation animation = CCAnimation.animation("deleteBone");
    for( int i=0;i<9;i++)
			animation.addFrame(String.format("deleteBone/%d.png",i+1));
   // animation.setDelayPerUnit(0.2f);
	CCAnimate action = CCAnimate.action(1,animation, false);
	//this.deleteBullet.runAction(action);
	CCSequence sequence=CCSequence.actions(action,CCCallFuncND.action(this,"resetBoneVisible"));
	this.deleteBone.runAction(sequence);
}




public void resetVisible(Object Sender){
	this.deleteBullet.setVisible(false);
}



public void resetBoneVisible(Object Sender){
	this.deleteBone.setVisible(false);
}


public CCSprite getDeleteBullet() {
	return deleteBullet;
}


public CCSprite getDeleteBone() {
	return deleteBone;
}


	
}
