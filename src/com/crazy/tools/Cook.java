package com.crazy.tools;

import org.cocos2d.actions.instant.CCCallFuncND;
import org.cocos2d.actions.interval.CCAnimate;
import org.cocos2d.actions.interval.CCMoveBy;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.actions.interval.CCSpawn;
import org.cocos2d.nodes.CCAnimation;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import com.crazy.service.GameLayer;
import com.crazy.ui.Common;

public class Cook  {
	
private int colume;
private int row;
private CCSprite cookSprite;     //厨师精灵


private Point centerCoordinate;   //厨师图片中心坐标
private boolean isMove;         //cook是否正在移动
private boolean aboutMove;        //是否拖动虚拟手柄准备移动
private int money;           //财富值
private int speed;
private int fire;         //武器等级（1  攻击一次敌方受击次数+1；2   攻击一次敌方受击次数+2；  攻击一次敌方受击次数+3）
private int size;          //厨师形态大小(0 正常大小；1 变换后的大小)
private int beHit;          //已受击次数
private int hitMax;        //最高能承受的受击次数
private int giftSituation;      //是否捡到gift以及捡到的gift种类（0  未装备；1 加血；2 盾牌；3 四面发射包子 ；4 使敌方静止）
private int direction;         //厨师的行进方向（0  上；1  右；2  下；3  左;5   未定义方向）
private int halfHeight;     //cook图片高度的一半
private int halfWidth;

public Cook(int beginColume, int beginRow,int beginDirection,int hitMax){
this.colume = beginColume;
this.row = beginRow;
this.money=GameLayer.initMoney;
this.speed = 1;
this.fire=1;
this.size = 0;
this.beHit = 0;
this.hitMax = hitMax;
this.giftSituation = 0;
this.direction = beginDirection;
this.isMove = false;
this.aboutMove=false;

this.cookSprite = CCSprite.sprite("walkManA/20.png");
this.centerCoordinate = new Point();
CGRect rect = cookSprite.getBoundingBox();
this.halfHeight = (int) (CGRect.height(rect)/2);
this.halfWidth = (int)(CGRect.width(rect)/2);
centerCoordinate.setX(beginColume*120+this.halfWidth);
centerCoordinate.setY(beginRow*120+this.halfHeight);
this.cookSprite.setPosition(centerCoordinate.getX(),centerCoordinate.getY());
//this.addChild(cookSprite);
}



public int getGiftSituation() {
	return giftSituation;
}



public void setGiftSituation(int giftSituation) {
	this.giftSituation = giftSituation;
}



public int getMoney() {
	return money;
}



public void setMoney(int money) {
	this.money = money;
}



//重置cook位置
public void resetCookPosition(int x,int y){
	this.cookSprite.setPosition(x,y);
}

//显示Cook
//public void showCook(){
//	this.addChild(cookSprite);
//}

//根据方向设置行走动画
public void createCookAnimation(Map map,int controlDirection)
{
	if(this.aboutMove ==true&&(controlDirection!=5)){
		if(this.isMove==true)
			return;	
	  CGPoint moveByPosition;
	  /*
	   * 
	   * 检测cook即将走到的位置是否有障碍物
	   * 以及设置不同方向的moveByPostion
	   */
	  CGPoint cookPosition = this.cookSprite.getPosition();
	  this.colume=(int)(cookPosition.x-Common.getSpriteWidth(this.cookSprite))/120;
	  this.row=(int)(cookPosition.y-Common.getSpriteHeight(this.cookSprite))/120;
	  boolean isMovable=false;
	 switch (controlDirection) {
     case 2:
    	 if(this.row-1>=0){
    		 if((map.getMapContent(this.colume, this.row-1)<1)||(map.getMapContent(this.colume, this.row-1)>6))
    		 {
    	      isMovable=true;   
    		 this.direction=controlDirection;
    		 }
    		 }
    	 moveByPosition=CGPoint.ccp(0,-120);
         break;
     case 3:
    	 if(this.colume-1>=0){
    		 if((map.getMapContent(this.colume-1, this.row)<1)||(map.getMapContent(this.colume-1, this.row)>6))
    		 {
    			 isMovable=true;
    		 	 this.direction=controlDirection;
    		 }
    	 	}
    	 moveByPosition=CGPoint.ccp(-120,0);
         break;
     case 1:
    	 if(this.colume+1<15){
    		 if((map.getMapContent(this.colume+1, this.row)<1)||(map.getMapContent(this.colume+1, this.row)>6))
    			 {
    			 isMovable=true;
    			 this.direction=controlDirection;
    			 }
    		 }
    	 moveByPosition=CGPoint.ccp(120,0);
         break;
     case 0:
    	 if(this.row+1<8){
    		 if((map.getMapContent(this.colume, this.row+1)<1)||(map.getMapContent(this.colume, this.row+1)>6))
    		 {
    			 isMovable=true;
    		   this.direction=controlDirection;
    		 }
    		 }
    	 moveByPosition=CGPoint.ccp(0,120);
         break;
     default:
    	 moveByPosition=CGPoint.ccp(0,0);
         break;
 }
	    //  animation.addFrame(String.format("walkManA/%d%d.png", 0,i));
	//animation.addFrame(String.format("walkManA/%d%d.png",1,i));
	//animation.addFrame(String.format("walkManA/%d%d.png", 3,i));
	
   // CCAnimation animation = CCAnimation.animation("walk");
  //  for( int i=0;i<4;i++)
		//	animation.addFrame(String.format("walkManA/%d%d.png",this.direction,i));
  
	//CCAnimate action = CCAnimate.action(1,animation, false);
	//CCRepeatForever forever = CCRepeatForever.action(action);
    //this.cookSprite.runAction(forever);  
	 //this.cookSprite.runAction(action); 
    
 // CCSequence meanTimeAction =CCSequence.actions(CCSpawn.actions(createAnimationByDirection(this.direction),CCMoveBy.action(1.12f,moveByPosition)), CCCallFuncND.action(this,"onWalkDone"));
  
 // CCSpawn spawn= CCSpawn.actions(action,CCMoveBy.action(0.28f,moveByPosition));
	//CCRepeatForever forever = CCRepeatForever.action(CCSequence.actions(CCSpawn.actions(createAnimationByDirection(direction),CCMoveBy.action(0.84f,moveByPosition)), CCCallFuncND.action(this,"onWalkDone")));
   //this.cookSprite.runAction(forever); 
  
  
  
  if(isMovable==true){
  CCSequence meanTimeAction =CCSequence.actions(CCSpawn.actions(createAnimationByDirection(this.direction),CCMoveBy.action(1.12f,moveByPosition)), CCCallFuncND.action(this,"onWalkDone"));
  this.cookSprite.runAction(meanTimeAction);
  this.setMove(true);
  }
	}
}

public int getBeHit() {
	return beHit;
}



public void setBeHit(int beHit) {
	this.beHit = beHit;
}



public void onWalkDone(Object Sender){
	this.setMove(false);
}

public CCAnimate createAnimationByDirection(int direction){
	  CCAnimation animation = CCAnimation.animation("walk");
	    for( int i=0;i<4;i++)
				animation.addFrame(String.format("walkManA/%d%d.png",direction,i));
	   // animation.setDelayPerUnit(0.2f);
		CCAnimate action = CCAnimate.action(1,animation, false);
	return action; 
}


public boolean isMove() {
	/*CCSprite sprite =CCSprite.sprite("old.png");
	CCTexture2D newTexture;

	CCSprite sprite1 = CCSprite.sprite("new.png");

	newTexture = sprite1.getTexture();


	 //sprite.stopAllActions();

	 sprite.setTexture(newTexture);*/
	
	return isMove;
}


public void setMove(boolean isMove) {
	this.isMove = isMove;
}




public int getDirection() {
	return direction;
}


public void setDirection(int direction) {
	this.direction = direction;
}


public CCSprite getCookSprite() {
	return cookSprite;
}



public boolean isAboutMove() {
	return aboutMove;
}


public void setAboutMove(boolean aboutMove) {
	this.aboutMove = aboutMove;
}



}
