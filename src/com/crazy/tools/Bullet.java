package com.crazy.tools;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.cocos2d.actions.base.CCRepeatForever;
import org.cocos2d.actions.instant.CCCallFuncND;
import org.cocos2d.actions.interval.CCMoveBy;
import org.cocos2d.actions.interval.CCMoveTo;
import org.cocos2d.actions.interval.CCRotateBy;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.actions.interval.CCSpawn;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteSheet;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import com.crazy.ui.Common;
import com.crazy.service.GameLayer;

public class Bullet {
	public static final int kTagSpriteManager = 1;
	public static int initBulletNum = 200;
	public static int initBoneNum[] ={500,500,500};
	private  ArrayList<CCSprite> idleBulletList = new ArrayList<CCSprite>();
	private ArrayList<CCSprite>	runBulletList = new ArrayList<CCSprite>();
	private CCSpriteSheet bulletSheet;
	//private int cookBulletDirection;
	private ArrayList<CCSprite> idleBoneList=new ArrayList<CCSprite>();
	private ArrayList<CCSprite> runBoneList=new ArrayList<CCSprite>();
	private CCSpriteSheet boneSheet;
	private int level=0;
	
	
public Bullet(){
	
}
	
	
	
	
public void initIdleBulletSpool(GameLayer  gameLayer,int level)	{
	
	this.level=level;
	/*
	 * 厨师的子弹
	 */
    bulletSheet= CCSpriteSheet.spriteSheet("bullet/bullet0.png");
	gameLayer.addChild(bulletSheet);
    //gameLayer.addChild(bulletSheet,0,kTagSpriteManager);
	bulletSheet.setPosition(0,0);
	//CCSpriteSheet sheet = (CCSpriteSheet)getChildByTag(kTagSpriteManager);
	gameLayer.addChild(bulletSheet);
	for(int i=0;i<initBulletNum;i++){
		CCSprite bullet=CCSprite.sprite(bulletSheet,CGRect.make(0,0,40,40));
		bullet.setTag(5);
		this.idleBulletList.add(bullet);
		bullet.setVisible(false);
		bulletSheet.addChild(bullet);	}
	
	/*
	 * 狗的子弹
	 */
	boneSheet=CCSpriteSheet.spriteSheet("bullet/dogBullet0.png");
	gameLayer.addChild(boneSheet);
	boneSheet.setPosition(0,0);
	gameLayer.addChild(boneSheet);
	for(int i=0;i<initBoneNum[level];i++){
		CCSprite bone=CCSprite.sprite(boneSheet,CGRect.make(0,0,55,40));
		bone.setTag(5);
		this.idleBoneList.add(bone);
		bone.setVisible(false);
		boneSheet.addChild(bone);
	}
	
	
	
}
	
public CCSprite getCookBullet(GameLayer gameLayer){	
	if(idleBulletList.size()>0){
		this.runBulletList.add(idleBulletList.get(this.idleBulletList.size()-1));
		this.idleBulletList.remove(this.idleBulletList.size()-1);
		//CCSprite newBullet= this.runBulletList.get(this.runBulletList.size());
		}
	else
		{
			CCSprite extraBullet =CCSprite.sprite(bulletSheet,CGRect.make(0,0,40,40));
			extraBullet.setTag(5);
			this.runBulletList.add(extraBullet);	
			extraBullet.setVisible(false);
			bulletSheet.addChild(extraBullet);
		}
	return this.runBulletList.get(this.runBulletList.size()-1);
	}

public CCSprite getBoneBullet(GameLayer gameLayer){	
	if(idleBoneList.size()>0){
		this.runBoneList.add(idleBoneList.get(this.idleBoneList.size()-1));
		this.idleBoneList.remove(this.idleBoneList.size()-1);
		//CCSprite newBullet= this.runBulletList.get(this.runBulletList.size());
		}
	else
		{
			CCSprite extraBone =CCSprite.sprite(boneSheet,CGRect.make(0,0,55,40));
			extraBone.setTag(5);
			this.runBulletList.add(extraBone);	
			extraBone.setVisible(false);
			bulletSheet.addChild(extraBone);
		}
	return this.runBoneList.get(this.runBoneList.size()-1);
	}


public void deleteCookBullet(int index,GameLayer gameLayer){
	CCSprite deleteBullet = this.runBulletList.get(index);
	this.runBulletList.remove(index);
	deleteBullet.setVisible(false);
	this.bulletSheet.removeChild(deleteBullet,true);
	gameLayer.removeChild(deleteBullet,true);
	
}


public void deleteBone(int index,GameLayer gameLayer){
	CCSprite deleteBullet = this.runBoneList.get(index);
	this.runBoneList.remove(index);
	deleteBullet.setVisible(false);
	this.boneSheet.removeChild(deleteBullet,true);
	gameLayer.removeChild(deleteBullet,true);
	
}





public ArrayList<CCSprite> getRunBoneList() {
	return runBoneList;
}




public void createCookBulletAnimation(CCSprite sprite,int direction){
	if(direction==5)
		direction=2;
	CGPoint moveBulletPos;
	switch(direction)
	{
		case 0:	moveBulletPos=CGPoint.ccp(0,300);
		 	break;
		case 1: moveBulletPos=CGPoint.ccp(300,0);
		    break;
		case 2:	moveBulletPos=CGPoint.ccp(0,-300);
			break;
		case 3:	moveBulletPos=CGPoint.ccp(-300,0);
			break;
		default:moveBulletPos=CGPoint.ccp(0,0);
				break;
	}
	
	sprite.setTag(direction);
	CCMoveBy moveBy=CCMoveBy.action(1f,moveBulletPos);
	CCSequence bulletSequence=CCSequence.actions(moveBy, CCCallFuncND.action(this,"onFireDone"));
	CCRepeatForever forever= CCRepeatForever.action(moveBy);
	sprite.runAction(forever);
	
	}


public void createBoneAnimation(CCSprite sprite,int direction){
	if(direction==5)
		direction=2;
	CGPoint moveBulletPos;
	switch(direction)
	{
		case 0:	moveBulletPos=CGPoint.ccp(0,240);
		 	break;
		case 1: moveBulletPos=CGPoint.ccp(240,0);
		    break;
		case 2:	moveBulletPos=CGPoint.ccp(0,-240);
			break;
		case 3:	moveBulletPos=CGPoint.ccp(-240,0);
			break;
		default:moveBulletPos=CGPoint.ccp(0,0);
				break;
	}
	
	sprite.setTag(direction);
	CCMoveBy moveBy=CCMoveBy.action(1f,moveBulletPos);
	 CCRotateBy  actionBy = CCRotateBy.action(1f , 540);
	CCSequence bulletSequence=CCSequence.actions(CCSpawn.actions(actionBy,moveBy), CCCallFuncND.action(this,"onFireDone"));
	CCRepeatForever forever= CCRepeatForever.action(bulletSequence);
	sprite.runAction(forever);
	
	}



public void setBulletPos(CCSprite sprite,CCSprite cook,int direction){
	if(direction==5){
		direction=2;
	}
	switch(direction){

		case 0:	sprite.setPosition(cook.getPosition().x,cook.getPosition().y+Common.getSpriteHeight(sprite)+Common.getSpriteHeight(cook));
			break;
		case 1: sprite.setPosition(cook.getPosition().x+Common.getSpriteWidth(sprite)+Common.getSpriteWidth(cook),cook.getPosition().y);
	    	break;
		case 2:	sprite.setPosition(cook.getPosition().x,cook.getPosition().y);
			break;
		case 3:	sprite.setPosition(cook.getPosition().x-Common.getSpriteWidth(sprite)-Common.getSpriteWidth(cook),cook.getPosition().y);
			break;
		default:
			break;
	}
	sprite.setVisible(true);
    sprite.setTag(direction);
}


public ArrayList<CCSprite> getRunBulletList() {
	return runBulletList;
}


private CCSpriteSheet getChildByTag(int ktagspritemanager2) {
	// TODO Auto-generated method stub
	return null;
}
	
	
	
	
	
	
}
