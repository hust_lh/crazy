package com.crazy.tools;

import org.cocos2d.actions.instant.CCCallFuncND;
import org.cocos2d.actions.interval.CCAnimate;
import org.cocos2d.actions.interval.CCMoveBy;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.actions.interval.CCSpawn;
import org.cocos2d.nodes.CCAnimation;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCTextureCache;
import org.cocos2d.opengl.CCTexture2D;
import org.cocos2d.types.CGPoint;

import android.util.Log;

import com.crazy.service.GameLayer;

public class Dog {
private int dogType;          //0   攻击力1;1  攻击力2
//private int fight;     //攻击力     其大小表示cook受到一次攻击时增加的被攻击数
private CCSprite dogSprite;
private int direction;
private int beHit;
private int colume;
private int row;
private int nextColume;
private int nextRow;
private boolean isGetRandomDirection;  //是否需要获取随机方向



private boolean isMove;
private boolean isBlock;  //表示狗是否和其它狗碰撞
private int checkFire;//0表示移动0步，1表示移动1步，2表示移动两步，每移动两步发射一个骨头



public Dog(GameLayer gameLayer, int type){
	this.dogType=type;
	//this.fight=type+1;
	this.beHit =0;
	this.isMove=false;
	this.checkFire=0;
	this.isBlock=false;
	this.isGetRandomDirection=true;    
	this.direction=(int)Math.random()*4;
}


public boolean isBlock() {
	return isBlock;
}


public void setBlock(boolean isBlock) {
	this.isBlock = isBlock;
}


public int calColume(){
	return (int)this.dogSprite.getPosition().x/120;
}

public int calRow(){
	return (int)this.dogSprite.getPosition().y/120;
}

public int calNextColume(){
	int tempColume;
	switch(this.direction)
	{
	case 0:tempColume=this.calColume();
		   break;
	case 1:tempColume=this.calColume()+1;
		   break;
	case 2:tempColume=this.calColume();
	       break;
	case 3:tempColume=this.calColume()-1;
	       break;
	default:tempColume=this.calColume();
		     break;
	}
	return tempColume;
}

public int calNextRow(){
	int tempRow;
	switch(this.direction)
	{
	case 0:tempRow=this.calRow()+1;
		   break;
	case 1:tempRow=this.calRow();
		   break;
	case 2:tempRow=this.calRow()-1;
	       break;
	case 3:tempRow=this.calRow();
	       break;
	default:tempRow=this.calRow();
		     break;
	}
	return tempRow;
}





public void dogMoveAnimation(GameLayer gameLayer){
	if(this.isMove==true)
		return;	
	 //CCTexture2D  newTexture = CCTextureCache.sharedTextureCache().addImage(String.format("dog/%d%d.png",this.dogType,this.direction));
//      this.dogSprite.setTexture(newTexture);
	CGPoint  moveBy;
	switch(direction){
	case 0: moveBy=CGPoint.ccp(0,120);
		    break;
	case 1:moveBy=CGPoint.ccp(120,0);
			break;
	case 2:moveBy=CGPoint.ccp(0,-120);
		   break;
	case 3:moveBy=CGPoint.ccp(-120,0);
		   break;
	default:moveBy=CGPoint.ccp(0,0);
		   break;
	}
	
	//this.dogSprite.runAction(CCMoveBy.action(1f,moveBy));
	if(isMove==false){
		  /*CCTexture2D newTexture;
           CCSprite sprite = CCSprite.sprite(String.format("dog/%d%d.png",this.dogType,direction));
           Log.e("换贴图","");
           newTexture = sprite.getTexture();
       //sprite.stopAllActions();
        this.dogSprite.setTexture(newTexture);*/
		
		
		/* CCAnimation animation = CCAnimation.animation("walk");
					animation.addFrame(String.format("dog/%d%d.png",this.dogType,this.direction));
		   // animation.setDelayPerUnit(0.2f);
			CCAnimate action = CCAnimate.action(1,animation, false);
		  CCSequence meanTimeAction =CCSequence.actions(CCSpawn.actions(action,CCMoveBy.action(1f,moveBy)), CCCallFuncND.action(this,"onMoveDone"));*/
		CCSequence meanTimeAction =CCSequence.actions(CCMoveBy.action(1f,moveBy), CCCallFuncND.action(this,"onMoveDone"));
		 CCTexture2D  newTexture = CCTextureCache.sharedTextureCache().addImage(String.format("dog/%d%d.png",this.dogType,this.direction));
     this.dogSprite.setTexture(newTexture);
		this.dogSprite.runAction(meanTimeAction);
		  this.isMove=true;
		  this.checkFire++;
		  if(this.checkFire==6){
		  CCSprite tempSprite =gameLayer.bulletLevelOne.getBoneBullet(gameLayer);
			tempSprite.setVisible(true);
			gameLayer.bulletLevelOne.setBulletPos(tempSprite,this.dogSprite,this.direction);
			gameLayer.bulletLevelOne.createBoneAnimation(tempSprite,tempSprite.getTag());
			this.checkFire =0;
		  }
	}
}


public void onMoveDone(Object Sender){
	this.isMove=false;
}
public void beginDogAnimation(GameLayer gameLayer){
	
}
public void killDogAnimation(GameLayer gameLayer){
	
}


public CCSprite getDogSprite() {
	return dogSprite;
}


public void setDogSprite(CCSprite dogSprite) {
	this.dogSprite = dogSprite;
}





public int getDirection() {
	return direction;
}





public void setDirection(int direction) {
	this.direction = direction;
}


public int getDogType() {
	return dogType;
}


public void setDogType(int dogType) {
	this.dogType = dogType;
}


public int getBeHit() {
	return beHit;
}


public void setBeHit(int beHit) {
	this.beHit = beHit;
}

public boolean isGetRandomDirection() {
	return isGetRandomDirection;
}


public void setGetRandomDirection(boolean isGetRandomDirection) {
	this.isGetRandomDirection = isGetRandomDirection;
}


/*
public int getFight() {
	return fight;
}*/
}
