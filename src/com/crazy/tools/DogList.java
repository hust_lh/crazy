package com.crazy.tools;

import java.util.ArrayList;

import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteSheet;
import org.cocos2d.nodes.CCTextureCache;
import org.cocos2d.opengl.CCTexture2D;
import org.cocos2d.types.CGRect;

import com.crazy.service.GameLayer;

/*
 * 根据关卡来产生对应的狗的动态数组
 */
public class DogList {
private ArrayList<Dog> levelOneDog1;          //空闲dog1动态数组
private ArrayList<Dog> levelOneDog2;           //空闲dog2动态数组
private ArrayList<Dog> runDog1;               //出现在游戏界面的dog1动态数组
private ArrayList<Dog> runDog2;               //出现在游戏界面的dog2动态数组

private CCSpriteSheet levelOneDog1Sheet;	

private CCSpriteSheet levelOneDog2Sheet;
	
public DogList(){
	
}
	
public void initDogList(GameLayer gameLayer,int level){
	
		levelOneDog1=new ArrayList(GameLayer.allDogNum1[level]);
		levelOneDog2=new ArrayList(GameLayer.allDogNum2[level]);
		runDog1=new ArrayList(GameLayer.runDogNum1[level]);
		runDog2=new ArrayList(GameLayer.runDogNum2[level]);
		//levelOneDog1Sheet= CCSpriteSheet.spriteSheet("dog/02.png");
		//levelOneDog2Sheet= CCSpriteSheet.spriteSheet("dog/12.png");
		//gameLayer.addChild(levelOneDog1Sheet);
		//gameLayer.addChild(levelOneDog2Sheet);
	//	levelOneDog1Sheet.setPosition(0,0);
	//	levelOneDog2Sheet.setPosition(0,0);
		//gameLayer.addChild(levelOneDog1Sheet);
		//gameLayer.addChild(levelOneDog2Sheet);
		
		
		for(int i=0;i<GameLayer.allDogNum1[level];i++)
		{
		Dog dog= new Dog(gameLayer, 0);
	   // dog.setDogSprite(CCSprite.sprite(levelOneDog1Sheet,CGRect.make(0,0,120,120)));
		dog.setDogSprite(CCSprite.sprite("dog/02.png"));
	// CCTexture2D  newTexture = CCTextureCache.sharedTextureCache().addImage(String.format("dog/00.png"));
	    // dog.getDogSprite().setTexture(newTexture);
		dog.setDirection(2);
		
		this.levelOneDog1.add(dog);
		dog.getDogSprite().setVisible(false);
		//levelOneDog1Sheet.addChild(dog.getDogSprite());
		gameLayer.addChild(dog.getDogSprite());
	    }
		
		
		
		for(int i=0;i<GameLayer.allDogNum2[level];i++)
		{
		Dog dog1= new Dog(gameLayer, 1);
	   // dog.setDogSprite(CCSprite.sprite(levelOneDog2Sheet,CGRect.make(0,0,120,120)));
		dog1.setDogSprite(CCSprite.sprite("dog/12.png"));
		dog1.setDirection(2);
		this.levelOneDog2.add(dog1);
		dog1.getDogSprite().setVisible(false);
		//levelOneDog2Sheet.addChild(dog.getDogSprite());
		gameLayer.addChild(dog1.getDogSprite());
	    }
		
		
	
}


public Dog  getNewDog(GameLayer gameLayer)
{
	int random=(int)(Math.random()*100)%2;
	if((random==0)&&(this.levelOneDog1.size()<1))
		random=1;
	if((random==1)&&(this.levelOneDog2.size()<1))
		random=0;
	if(random==0){
		this.runDog1.add(this.levelOneDog1.get(this.levelOneDog1.size()-1));
		this.levelOneDog1.remove(this.levelOneDog1.size()-1);
		GameLayer.newDog=random;
		return this.runDog1.get(this.runDog1.size()-1);
	}
	else{
		this.runDog2.add(this.levelOneDog2.get(this.levelOneDog2.size()-1));
		this.levelOneDog2.remove(this.levelOneDog2.size()-1);
		GameLayer.newDog=random;
		return this.runDog2.get(this.runDog2.size()-1);
	}
    //gameLayer.schedule("dogFire",1f);
	
}

/*
 * 狗和狗，以及狗和人之间的碰撞检测
 */
/*public boolean isTouch(){
	for (int i=0;i<this.runDog1.size();i++)
	{
		for(int j=0;j<this.runDog1.size();j++)
		{
			if(j!=i)
			{
				if(this.runDog1.get(j).calColume()==this.runDog1.get(i).calColume())
				{
					if(Math.abs(this.runDog1.get(j).calRow()-this.runDog1.get(i).calRow())<=120)
					{
						return false;
					}
				}
				
				if(this.runDog1.get(j).calRow()==this.runDog1.get(i).calColume())
				{
					if(Math.abs(this.runDog1.get(j).calRow()-this.runDog1.get(i).calRow())<=120)
					{
						return false;
					}
				}
				
				
			}
		}
	}
}
*/


public void deleteDog(int type,int index,GameLayer gameLayer){
	CCSprite deleteDog;
	if(type==0)
	{
		deleteDog = this.runDog1.get(index).getDogSprite();
		this.runDog1.remove(index);
		deleteDog.setVisible(false);
		this.levelOneDog1Sheet.removeChild(deleteDog,true);	
		gameLayer.removeChild(deleteDog,true);
	}
	if(type==1)
	{
		 deleteDog = this.runDog2.get(index).getDogSprite();
		this.runDog2.remove(index);
		deleteDog.setVisible(false);
		this.levelOneDog2Sheet.removeChild(deleteDog,true);
		gameLayer.removeChild(deleteDog,true);
	}
	
}







public ArrayList<Dog> getLevelOneDog1() {
	return levelOneDog1;
}

public ArrayList<Dog> getLevelOneDog2() {
	return levelOneDog2;
}

public ArrayList<Dog> getRunDog1() {
	return runDog1;
}

public ArrayList<Dog> getRunDog2() {
	return runDog2;
}




}
