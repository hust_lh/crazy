package com.crazy.tools;

import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGRect;

public class Map {
	
	private  int array[][];
	private int level; 
	private int certainMapContent;
	private CCSprite mapSprite;
	
	private int columeMax;
	private int rowMax;
	private int halfHeight;
	private int halfWidth;
	
	public Map(int arrayImput[][],int columeMax,int rowMax,int level){
		
		this.array=new int [rowMax][columeMax];
		this.columeMax = columeMax;
		this.rowMax = rowMax;
		this.certainMapContent=0;
		this.level = level;
		this.mapSprite = CCSprite.sprite(String.format("mapFolder/map%d.png",level));
		CGRect rect = mapSprite.getBoundingBox();
		this.halfHeight = (int) (CGRect.height(rect)/2);
		this.halfWidth = (int)(CGRect.width(rect)/2);
		this.mapSprite.setPosition(halfWidth,halfHeight);
		//this.addChild(mapSprite);
		
		for(int i=0;i<rowMax;i++){
			
			for (int j=0;j<columeMax;j++)
			{
				this.array[i][j]=arrayImput[i][j];
			}
		}
	
	}
	 
	
	
	
	public int getMapContent(int certainColume,int certainRow){
		
		this.certainMapContent=this.array[certainRow][certainColume];
		return this.certainMapContent;
	}
	
     public void setMapContent(int certainColume,int certainRow,int value){
		
	    this.array[certainRow][certainColume]=value;
	}
	
	
	
	
	public int getColumeMax() {
		return columeMax;
	}
	
	public int getRowMax() {
		return rowMax;
	}
	
	public CCSprite getMapSprite() {
		return mapSprite;
	}
	
	public int getHalfWidth() {
		return halfWidth;
	}
	
	
	

}
