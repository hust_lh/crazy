package com.crazy.tools;

import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteSheet;

public class ObstacleLevelOne {
private CCSprite boxSprite[];
private CCSprite stoolSprite[];
private CCSprite stoneSprite[];
private  int boxGrid[][];
private  int stoolGrid[][];
private  int stoneGrid[][];
private int boxNum;
private int stoolNum;
private int stoneNum;
private  CCSpriteSheet boxSheet;
private CCSpriteSheet stoolSheet;
private CCSpriteSheet stoneSheet;

//private CCSpriteBatchNode boxBatch;


public ObstacleLevelOne(int boxNum,int stoolNum,int stoneNum){
	this.boxNum=boxNum;
	this.stoolNum=stoolNum;
	this.stoneNum=stoneNum;
	this.boxSprite=new CCSprite[boxNum];
	this.stoolSprite = new CCSprite[stoolNum];
	this.stoneSprite = new CCSprite[stoneNum];
	this.boxGrid =new int [boxNum][2];
	this.stoolGrid =new int [stoolNum][2];
	this.stoneGrid = new int [stoneNum][2];
	for(int i=0;i<boxNum;i++){
		this.boxSprite[i] = CCSprite.sprite("obstacle/obstacleBox.png");
	}
	for(int j=0;j<stoolNum;j++){
		this.stoolSprite[j] = CCSprite.sprite("obstacle/obstacleStool.png");
	}
	for(int k=0;k<stoneNum;k++){
		this.stoneSprite[k] = CCSprite.sprite("obstacle/obstacleStone.png");
	}
	
	
	//利用CCSpriteSheet批处理障碍物
	/*boxSheet = CCSpriteSheet.spriteSheet("obstacle/obstacleBox.png");
	boxSheet.setPosition(0,0);
	this.addChild()*/
	
	
	
	
	
	
	
	
	
	
	
}


public int getBoxNum() {
	return boxNum;
}


public int getStoolNum() {
	return stoolNum;
}


public int getStoneNum() {
	return stoneNum;
}


public CCSprite getCertainBoxSprite(int num){
	return this.boxSprite[num];
}

public CCSprite getCertainStoolSprite(int num){
	return this.stoolSprite[num];
}
public CCSprite getCertainStoneSprite(int num){
	return this.stoneSprite[num];
}

public int getCertainBoxColume(int num){
	return this.boxGrid[num][0];
}

public int getCertainBoxRow(int num){
	return this.boxGrid[num][1];
}

public int getCertainStoolColume(int num){
	return this.stoolGrid[num][0];
}

public int getCertainStoolRow(int num){
	return this.stoolGrid[num][1];
}

public int getCertainStoneColume(int num){
	return this.stoneGrid[num][0];
}

public int getCertainStoneRow(int num){
	return this.stoneGrid[num][1];
}

public void setCertainBoxGrid(int num,int columeValue,int rowValue){
	 this.boxGrid[num][0]=columeValue;
	 this.boxGrid[num][1]=rowValue;
}


public void setCertainStoolGrid(int num,int columeValue,int rowValue){
	 this.stoolGrid[num][0]=columeValue;
	 this.stoolGrid[num][1]=rowValue;
}



public void setCertainStoneGrid(int num,int columeValue,int rowValue){
	 this.stoneGrid[num][0]=columeValue;
	 this.stoneGrid[num][1]=rowValue;
}







}
