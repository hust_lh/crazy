package com.crazy.service;

import org.cocos2d.types.CGRect;

import com.crazy.tools.Dog;
import com.crazy.tools.DogList;

public class Assistant {
	
	
	
	
	
	
	
	
	
	
	public Assistant(){
		
	}
	
	/*
	 * 根据圆心和触摸点的位置计算弧度
	 */
	public double getRad(float px1, float py1, float px2, float py2) {  
	    //得到两点X的距离  
	    float x = px2 - px1;  
	    //得到两点Y的距离  
	    float y = py1 - py2;  
	    //算出斜边长  
	    float xie = (float) Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));  
	    //得到这个角度的余弦值（通过三角函数中的定理 ：邻边/斜边=角度余弦值）  
	    float cosAngle = x / xie;  
	    //通过反余弦定理获取到其角度的弧度  
	    float rad = (float) Math.acos(cosAngle);  
	    //注意：当触屏的位置Y坐标<摇杆的Y坐标要取反值-0~-180  
	    if (py2 < py1) {  
	        rad = -rad;  
	    }  
	    return rad;  
	}  
	
	public boolean isTouch(CGRect rectA,CGRect rectB){
		/*if(rectA==rectB)
		{
			return false;
		}*/
		 float left = Math.max(CGRect.minX(rectA), CGRect.minX(rectB));  
		 float right = Math.min(CGRect.maxX(rectA),CGRect.maxX(rectB));  
		 if (left > right)  
		        return false;  
	     float top = Math.min(CGRect.maxY(rectA),CGRect.maxY(rectB));  
		 float bottom = Math.max(CGRect.minY(rectA), CGRect.minY(rectB));  
		 if (top < bottom)  
		        return false;  		  
		    return true;  
	}
	
	public boolean isTouchDog(int type,int index,DogList dogList){
		//CGRect rectA=CGRect.make(dogList.getRunDog1().get(index).calNextColume()*120,dogList.getRunDog1().get(index).calNextRow()*120,120,120);
		//CGRect rectB=CGRect.make(dogList.getRunDog2().get(index).calNextColume()*120,dogList.getRunDog2().get(index).calNextRow()*120,120,120);
		
		
	
		
		if((dogList.getRunDog1().size()+dogList.getRunDog2().size())>1)
		{	
			//CGRect rectA=dogList.getRunDog1().get(index).getDogSprite().getBoundingBox();
	       //	CGRect rectB=dogList.getRunDog2().get(index).getDogSprite().getBoundingBox();
			
			if(type==0)
			{
				CGRect rectA=dogList.getRunDog1().get(index).getDogSprite().getBoundingBox();
		if(dogList.getRunDog1().size()>1){
			for(int i=0;i<dogList.getRunDog1().size();i++)
			{
				//if((i!=index)&&(dogList.getRunDog1().get(index).calNextColume()==dogList.getRunDog1().get(i).calColume())&&(dogList.getRunDog1().get(index).calNextRow()==dogList.getRunDog1().get(i).calRow()))
				
				//if((i!=index)&&(isTouch( rectA,dogList.getRunDog1().get(i).getDogSprite().getBoundingBox())))	
			if(i!=index){
				if(isTouch( rectA,dogList.getRunDog1().get(i).getDogSprite().getBoundingBox()))
				return true;
			}
			}
			}
		
		if(dogList.getRunDog2().size()>0){
			for(int j=0;j<dogList.getRunDog2().size();j++)
			{ 
				
				//CGRect rectB=CGRect.make(dogList.getRunDog1().get(index).calNextColume()*120,dogList.getRunDog1().get(index).calNextRow()*120,120,120);
			//if((dogList.getRunDog1().get(index).calNextColume()==dogList.getRunDog2().get(j).calColume())&&(dogList.getRunDog1().get(index).calNextRow()==dogList.getRunDog2().get(j).calRow()))
			if(isTouch( rectA,dogList.getRunDog2().get(j).getDogSprite().getBoundingBox()))
				
				return true;
			}
		
			}
			
		return false;
			}
			
			
			
			
			
			if(type==1)
			{
				CGRect rectB=dogList.getRunDog2().get(index).getDogSprite().getBoundingBox();
		if(dogList.getRunDog1().size()>0){
			for(int i=0;i<dogList.getRunDog1().size();i++)
			{
				//if((dogList.getRunDog2().get(index).calNextColume()==dogList.getRunDog1().get(i).calColume())&&(dogList.getRunDog2().get(index).calNextRow()==dogList.getRunDog1().get(i).calRow()))
			
				if(isTouch(rectB,dogList.getRunDog1().get(i).getDogSprite().getBoundingBox()))	
					return true;
			}
			}
		
		if(dogList.getRunDog2().size()>1){
			for(int j=0;j<dogList.getRunDog2().size();j++)
			{ 
			//if((j!=index)&&(dogList.getRunDog2().get(index).calNextColume()==dogList.getRunDog2().get(j).calColume())&&(dogList.getRunDog2().get(index).calNextRow()==dogList.getRunDog2().get(j).calRow()))
		  // if((j!=index)&&(isTouch(rectB,dogList.getRunDog2().get(j).getDogSprite().getBoundingBox())))
			 if(j!=index)
			 {
				if(isTouch(rectB,dogList.getRunDog2().get(j).getDogSprite().getBoundingBox()))
				return true;
			 }
			}
		
			}
			
		return false;
			}
			
			return false;
			
			
			
			
			
		}
		else return false;
	}	
	
}
