package com.crazy.service;

import java.util.ArrayList;
import java.util.List;

import org.cocos2d.Cocos2D;
import org.cocos2d.actions.CCTimer;
import org.cocos2d.actions.instant.CCCallFuncND;
import org.cocos2d.actions.interval.CCAnimate;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.events.CCTouchDispatcher;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.nodes.CCAnimation;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteSheet;
import org.cocos2d.nodes.CCTextureCache;
import org.cocos2d.opengl.CCTexture2D;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import android.util.Log;
import android.view.MotionEvent;

import com.crazy.tools.Bullet;
import com.crazy.tools.Cook;
import com.crazy.tools.Dog;
import com.crazy.tools.DogList;
import com.crazy.tools.Effect;
import com.crazy.tools.Map;
import com.crazy.tools.ObstacleLevelOne;
import com.crazy.ui.Common;



public class GameLayer extends CCLayer{
	
	//public static final int kTagSpriteManager = 1;
	public static int initMoney=0;                  //初始财富值
	public static int allDogNum1[]={8,9,10};        //每一关第一等级狗的初始化总数量
	public static int allDogNum2[]={4,6,8};         //每一关第二等级狗的初始化总数量
	
	public static int runDogNum1[]={4,5,6};              //每一关屏幕中最多可同时运行的第一等级狗的数量
	public static int runDogNum2[]={4,5,6};              //每一关屏幕中最多可同时运行的第二等级狗的数量
	public static int allRunDog[]={4,5,6};                //每一关屏幕中同时运行的所有狗的数量
	public static int level=0;             //0   关卡一；1  关卡二；2 关卡三；
	public static CGPoint bornPoint[][]={{CGPoint.ccp(300,780),CGPoint.ccp(1260,900),CGPoint.ccp(1380,300)},{CGPoint.ccp(180,900),CGPoint.ccp(1620,900),CGPoint.ccp(1620,300)},{CGPoint.ccp(180,900),CGPoint.ccp(1620,900),CGPoint.ccp(1620,300)}};       //每一关中生成敌方狗的位置
	private Common common = new Common();
	private Assistant assistant = new Assistant();
	private boolean canBorn;
	private boolean aboutBorn;
	//private boolean isGetRandomDirection;//是否需要获取随机方向
	private CCSprite gameBg;
	private Map mapLevelOne;
	private Cook cookSingle;
	public Bullet bulletLevelOne;
	private Effect effect;
	private ObstacleLevelOne obstacleOne;
	private CCSprite touchBack;         //virtualControl背景
	private CCSprite touch;             //virtualControl前景（控制点）
	//private CCSprite box[];
	//private CCSprite stool[];
	//private CCSprite stone[];
	
	//private ArrayList <CCSprite> box;
	//private ArrayList <CCSprite> stool;
	//private ArrayList <CCSprite> stone;
	CCSpriteSheet obstacleLevelOneSheet;
	CCSpriteSheet giftSheet;
	private ArrayList <CCSprite> mapObstacle;
	private ArrayList <CCSprite> gift;
	private CCSprite baseCamp;
	private CCSprite born;
	private DogList dogList;
	private int controlID;				//多点触控中控制虚拟手柄的触点ID    10表示无符合条件的触点
	private int fireID;               //多点触控中控制开火的触点ID    10表示无符合条件的触点
	private int fire;                /*
	                                  *0          开火控件未被触碰
	                                  *1          按下开火按钮
	                                  *2          释放开火按钮
	                                  */
	private int controlDirection;
	private int beforeRandomBornPos=4;   //上次狗的生成位置
	private int thisBorn;//生成狗的位置
	private long fireTouchBegan;
	private long fireTouchEnded;
	private CGPoint touchBgPoint;
	public static int newDog;
	
	
	
	
	/*private  int arrayImput[][]={{0,0,1,1,2,2,4,0,0,0,0,0,0,0,1},
			{4,3,0,0,4,0,0,0,0,0,4,0,2,3,0},
			{0,0,0,2,0,0,0,0,2,0,2,0,0,0,0},
			{1,3,0,2,0,0,4,0,0,0,2,0,1,1,3},
			{1,3,0,2,0,4,2,4,0,0,0,1,1,4,0},
			{0,0,0,0,0,4,5,4,0,2,0,4,2,0,0},
			{4,0,0,0,0,3,4,2,0,0,0,4,4,0,0},
			{3,0,1,1,0,0,0,0,0,0,0,0,0,3,2},
			{2,2,1,1,3,2,0,0,2,4,2,0,4,0,0}};*/

	public GameLayer() {
		// TODO Auto-generated constructor stub
		this.setIsTouchEnabled(true);
		
		gameInit();
		//gameLayout();
		
		gameBegin();
		
		
	}
	
	public void gameInit(){
		/*
		 * 0                   无障碍物
		 * 1                   无法消失的障碍物
		 * 2                    木箱
		 * 3                    木桩           
		 * 4                    石头
		 * 5                   被打了一半的石头
		 * 6                    大本营
		 * 
		 * ******gifts******
		 * 7                   加血
		 * 8                  盾牌（受击时受击次数不会增加）
		 * 9                   四面发射包子
		 * 10                  使敌方暂停
		 * 11                  财富值包子
		 */
		
		//this.arrayImput=new int [9][15];
		//this.cookBulletDirection=5;
		this.aboutBorn=true;
		this.canBorn=false;
	//	this.isGetRandomDirection=true;
		this.controlDirection=5;
		this.fire = 0;
		this.fireID=10;
		this.controlID =10;
		this.thisBorn =0;
        int imputArray[][]={{0,0,1,1,2,2,4,0,0,0,0,0,0,0,1},
		                    {4,0,0,4,4,4,0,0,2,0,4,0,2,3,1},
		                    {0,0,0,0,0,0,0,0,2,0,2,0,0,0,3},
		                    {1,3,0,2,0,0,4,0,0,0,2,0,1,1,3},
		                    {1,3,0,2,0,4,2,4,0,0,0,1,1,4,3},
		                    {4,0,0,2,0,4,6,4,0,2,0,4,2,0,0},
		                    {4,0,0,0,0,3,4,2,3,3,0,4,4,0,2},
		                    {3,0,1,1,0,0,0,0,0,0,0,0,0,0,0},
		                    {2,2,1,1,3,2,0,0,2,4,2,0,4,3,0}};
		
		
		this.mapLevelOne = new Map(imputArray,15,9,1);
		this.addChild(mapLevelOne.getMapSprite());
	    this.bulletLevelOne =new Bullet();
	   
	    this.dogList= new DogList();
	    
	  //  CCTouchDispatcher.sharedDispatcher().addDelegate(this, 0);
	    
	    
	/*
		this.obstacleOne = new ObstacleLevelOne(20,12,20);
		int temp1=0; 
		int temp2=0;
		int temp3=0;
		for(int i=0;i<mapLevelOne.getRowMax();i++){
			for(int j=0;j<mapLevelOne.getColumeMax();j++){
				if(mapLevelOne.getMapContent(j, i)==2)
					{
					obstacleOne.setCertainBoxGrid(temp1,j,i);
					temp1++;
					}
				else if(mapLevelOne.getMapContent(j, i)==3)
				{
					obstacleOne.setCertainStoolGrid(temp2,j,i);
					temp2++;
				}
				else if(mapLevelOne.getMapContent(j, i)==4)
				{
					obstacleOne.setCertainStoneGrid(temp3,j,i);
					temp3++;
				}
				
			}
		}
			
		for(int k=0;k<obstacleOne.getBoxNum();k++){
			obstacleOne.getCertainBoxSprite(k).setPosition(60+120*obstacleOne.getCertainBoxColume(k),
					60+120*obstacleOne.getCertainBoxRow(k));
			this.addChild(obstacleOne.getCertainBoxSprite(k));
		}
		for(int o=0;o<obstacleOne.getStoolNum();o++){
			obstacleOne.getCertainStoolSprite(o).setPosition(60+120*obstacleOne.getCertainStoolColume(o),
					60+120*obstacleOne.getCertainStoolRow(o));
			this.addChild(obstacleOne.getCertainStoolSprite(o));
		}
		for(int p=0;p<obstacleOne.getStoneNum();p++){
			obstacleOne.getCertainStoneSprite(p).setPosition(60+120*obstacleOne.getCertainStoneColume(p),
					60+120*obstacleOne.getCertainStoneRow(p));
			this.addChild(obstacleOne.getCertainStoneSprite(p));
		}
		
		*/
		
		
		
	    giftSheet=CCSpriteSheet.spriteSheet("gift/gifts.png");
	    this.addChild(giftSheet);
	    giftSheet.setPosition(0,0);
	    this.gift=new ArrayList<CCSprite>();
		/*
		 * 	设置第一关的障碍物
		 */
	    obstacleLevelOneSheet = CCSpriteSheet.spriteSheet("obstacle/obstacleLevelOneSheet.png");
		//this.addChild(obstacleLevelOneSheet,0,kTagSpriteManager);
		this.addChild(obstacleLevelOneSheet);
		obstacleLevelOneSheet.setPosition(0,0);
		//this.box=new CCSprite[20];
		//this.stool =new CCSprite[12];
		//this.stone = new CCSprite[20];
		
		//this.box=new ArrayList<CCSprite>();
		//this.stool=new ArrayList<CCSprite>();
		//this.stone=new ArrayList<CCSprite>();
		this.mapObstacle =new ArrayList<CCSprite>();
		
		for(int i=0;i<mapLevelOne.getRowMax();i++){
			for(int j=0;j<mapLevelOne.getColumeMax();j++){
				if(mapLevelOne.getMapContent(j, i)==2)
					{
					CCSprite sprite=CCSprite.sprite(obstacleLevelOneSheet,CGRect.make(0,0,120,120));	
					//boxSheet.addChild(this.box[temp1]);
					sprite.setTag(2);
					obstacleLevelOneSheet.addChild(sprite);
					sprite.setPosition(60+120*j,60+120*i);
					this.mapObstacle.add(sprite);
					}
				else if(mapLevelOne.getMapContent(j, i)==3)
				{
					CCSprite sprite=CCSprite.sprite(obstacleLevelOneSheet,CGRect.make(120,0,120,120));
					sprite.setTag(3);
					obstacleLevelOneSheet.addChild(sprite);
					sprite.setPosition(60+120*j,60+120*i);
					this.mapObstacle.add(sprite);
				}
				else if(mapLevelOne.getMapContent(j, i)==4)
				{
					CCSprite sprite=CCSprite.sprite(obstacleLevelOneSheet,CGRect.make(240,0,120,120));
					sprite.setTag(4);
					obstacleLevelOneSheet.addChild(sprite);
					sprite.setPosition(60+120*j,60+120*i);
					this.mapObstacle.add(sprite);
				}
				else if(mapLevelOne.getMapContent(j, i)==6)
				{
					this.baseCamp=CCSprite.sprite("obstacle/BaseCamp.png");
					baseCamp.setPosition(60+120*j,60+120*i);
					this.addChild(baseCamp);
				}
				
			}
		}
	
		
	    born=CCSprite.sprite("bornAnimation/frame1.png");
		//born.setVisible(false);
		this.addChild(born);
		
		//添加cook精灵
				this.cookSingle = new Cook(7,2,5,6);
				this.addChild(cookSingle.getCookSprite());
				 //CCTexture2D  newTexture = CCTextureCache.sharedTextureCache().addImage(String.format("dog/00.png"));
				// this.cookSingle.getCookSprite().setTexture(newTexture);
		/*
		 * 
		 * 初始化子弹精灵和狗类动态数组
		 */
		
		this.dogList.initDogList(this, this.level);
		this.bulletLevelOne.initIdleBulletSpool(this,this.level);
		
		 this.effect=new Effect(this);
		
		//this.cookSingle.createCookAnimation();
		
		//添加虚拟手柄等控件
		touchBack = CCSprite.sprite("virtualControl/touchBg.png");
	    this.touchBgPoint = CGPoint.ccp(200, 200);
		touchBack.setPosition(touchBgPoint);
		this.addChild(touchBack);	
		touch = CCSprite.sprite("virtualControl/touch.png");
		CGPoint touchPoint = CGPoint.ccp(200, 200);
		touch.setPosition(touchPoint);
		this.addChild(touch);	
		
		
		
		
		//this.schedule("createDog",3);           //如果屏幕中运行的狗小于4个，则每隔2秒产生一个狗
		
		
	}
	 
	public void gameBegin(){
		this.schedule("createDog",1);           //如果屏幕中运行的狗小于4个，则每隔2秒产生一个狗
		//this.schedule("dogTouch",1/60);
		this.schedule("moveDog",0.3f);           //该计时器负责狗的移动
		this.schedule("checkBullet",0.2f);      //该计时器负责检测包子和骨头的状态并产生相应的效果
		//this.schedule("checkTouch",0.3f);         //处理狗和狗的碰撞
	   this.schedule("checkCookCondition",0.5f);    //处理cook状态以及胜利条件检测
		
	}
	
	
	public boolean ccTouchesBegan(MotionEvent event){
		
		int num = event.getPointerCount();
		//Log.e("began触碰点数",num+"");
		//System.out.println("began触碰点数"+num);
		for(int i=0;i<num;i++)
		{
			
		  CGPoint point= CCDirector.sharedDirector().convertToGL(CGPoint.ccp(event.getX(i), event.getY(i)));
	      CGRect rect=this.touchBack.getBoundingBox();
	      
	      if (CGRect.containsPoint(rect,point))  
	      {  
	          this.cookSingle.setAboutMove(true);           
	          this.controlID =event.getPointerId(i);
	          this.touch.setPosition(point);
	      }  
	     // else if((event.getX(i)>this.mapLevelOne.getHalfWidth())&&(event.getY(i)>150))
	      else if((point.x>this.mapLevelOne.getHalfWidth())&&(point.y<930))
	      {
	    	  this.fireTouchBegan=System.currentTimeMillis();
	    	  this.fire=1;
	    	  this.fireID =event.getPointerId(i);
	         // CCSprite tempSprite =this.bulletLevelOne.getCookBullet(this);
			  //tempSprite.setVisible(true);
			  //bulletLevelOne.setCookBulletPos(tempSprite, cookSingle.getCookSprite(),this.cookSingle.getDirection());
			  //bulletLevelOne.createCookBulletAnimation(tempSprite,tempSprite.getTag());
	    	  this.schedule("fire", 1);
	    	  this.fireID =event.getPointerId(i);
	      }
	    //  System.out.println("controlID-->"+this.controlID);
	    //  System.out.println("fireID-->"+this.fireID);
	     // Log.e("controlID-->",this.controlID+"");
	    //  Log.e("fireID-->",this.fireID+"");
		}
		
	
		return super.ccTouchesBegan(event);
	}
	
	public boolean ccTouchesMoved(MotionEvent event){
		
		int num = event.getPointerCount();
		//System.out.println("move触碰点数"+num);
		
		for(int i=0;i<num;i++)
		{
		if(event.getPointerId(i)==this.controlID)
		{
			CGPoint MovePoint= CCDirector.sharedDirector().convertToGL(CGPoint.ccp(event.getX(i), event.getY(i)));
			//判断手指触碰点是否在虚拟手柄的有效控制范围内
			if((Math.pow((MovePoint.x-touchBgPoint.x),2)+Math.pow((MovePoint.y-touchBgPoint.y),2))<16*Math.pow(common.getSpriteWidth(touchBack),2))
			{
				double rad;
				double pi=3.14159;
				rad = assistant.getRad(touchBgPoint.x,touchBgPoint.y,MovePoint.x,MovePoint.y);
				if((rad>(pi/4))&&(rad<(3*pi/4))&&(this.cookSingle.getDirection()!=0))
				{
					this.controlDirection=0;
				}
				else if(rad<(pi/4)&&rad>(-pi/4)&&(this.cookSingle.getDirection()!=1))
				{
					this.controlDirection=1;
				}
				else if(rad<(-pi/4)&&rad>(-3*pi/4)&&(this.cookSingle.getDirection()!=2))
				{
					this.controlDirection=2;
				}
				else if(((rad>(-pi)&&rad<(-3*pi/4))||(rad>(3*pi/4)&&(rad<=pi)))&&(this.cookSingle.getDirection()!=3))
				{
					this.controlDirection=3;
				}
				
				
				
				this.cookSingle.createCookAnimation(this.mapLevelOne,this.controlDirection);
				
			
				if((Math.pow((MovePoint.x-touchBgPoint.x),2)+Math.pow((MovePoint.y-touchBgPoint.y),2))<=Math.pow(common.getSpriteWidth(touchBack),2))
					this.touch.setPosition(MovePoint);
				else 
					{
					CGPoint MovePoint1 =CGPoint.ccp((float) (common.getSpriteWidth(touchBack) * Math.cos(rad)) + touchBgPoint.x,(float) (common.getSpriteWidth(touchBack) * Math.sin(rad))  + touchBgPoint.y);
					
					this.touch.setPosition(MovePoint1);
					}
			}
			else this.touch.setPosition(touchBgPoint.x,touchBgPoint.y);
				 
				
		}
		
		
		}

		return super.ccTouchesMoved(event);

	}
	
	public boolean ccTouchesEnded(MotionEvent event){
		int num = event.getPointerCount();
		CGPoint[] endPoint=new CGPoint[num];
		//System.out.println("end触碰点数"+num);
		//Log.e("end触碰点数",num+"");
		 if(num==1){
			 CGPoint endPointSingle= CCDirector.sharedDirector().convertToGL(CGPoint.ccp(event.getX(event.getPointerId(0)), event.getY(event.getPointerId(0))));
			    // if (event.getPointerId(i)==this.controlID)  
				//if((Math.pow((endPoint[i].x-touchBgPoint.x),2)+Math.pow((endPoint[i].y-touchBgPoint.y),2))<16*Math.pow(common.getSpriteWidth(touchBack),2))
			     if(endPointSingle.x<this.mapLevelOne.getHalfWidth())
				{  
			    	 
			          this.cookSingle.setAboutMove(false);
			          this.controlID =10;
			          this.touch.setPosition(touchBgPoint);
			         // Log.e("虚拟手柄放开ID-->",event.getPointerId(i)+"");
			          return super.ccTouchesEnded(event); 
			      }  
			     // else if(event.getPointerId(i)==this.fireID)
			     // else if((endPoint[i].x>this.mapLevelOne.getHalfWidth())&&(endPoint[i].y<930))
			     if(endPointSingle.x>this.mapLevelOne.getHalfWidth())
			      {
//			    	  System.out.println("开火放开-->"+endPoint.x); 
//			    	  System.out.println("开火放开ID-->"+event.getPointerId(i));
			    	//  Log.e("开火放开ID-->",event.getPointerId(i)+"");
			    	  this.fireTouchEnded=System.currentTimeMillis();
			    	  
			    	  if((this.fireTouchEnded-this.fireTouchBegan)<1000)
			    	  {
			    		  CCSprite tempSprite =this.bulletLevelOne.getCookBullet(this);
						  tempSprite.setVisible(true);
						  bulletLevelOne.setBulletPos(tempSprite, cookSingle.getCookSprite(),this.cookSingle.getDirection());
						  bulletLevelOne.createCookBulletAnimation(tempSprite,tempSprite.getTag()); 
			    	  }
			    	  this.fire=0;  
			    	  this.unschedule("fire");
			    	  this.fireID =10;                            //开火后将其置为10（即无触碰状态）
			    	  return super.ccTouchesEnded(event);
			      }
			      
				
		 }
		if(num==2)
	{
		for(int i=0;i<num;i++)
		{
		
		endPoint[i]= CCDirector.sharedDirector().convertToGL(CGPoint.ccp(event.getX(event.getPointerId(i)), event.getY(event.getPointerId(i))));
		//Log.e("pointID",event.getPointerId(i)+"");
		//Log.e("endPoint.x",endPoint[i].x+"");
	     // CGRect rect=this.touchBack.getBoundingBox();
	    // if (event.getPointerId(i)==this.controlID)  
		//if((Math.pow((endPoint[i].x-touchBgPoint.x),2)+Math.pow((endPoint[i].y-touchBgPoint.y),2))<16*Math.pow(common.getSpriteWidth(touchBack),2))
	     if(endPoint[i].x>this.mapLevelOne.getHalfWidth())
		{  
	    	 
	          this.cookSingle.setAboutMove(false);
	         // this.cookSingle.stopAction();
	          this.controlID =10;
	          this.touch.setPosition(touchBgPoint);
//	          System.out.println("虚拟手柄放开-->"+endPoint.x); //获取第i个点触控的y位置  
//	          System.out.println("虚拟手柄放开ID-->"+event.getPointerId(i));
	         // Log.e("虚拟手柄放开ID-->",event.getPointerId(i)+"");
	          return super.ccTouchesEnded(event); 
	      }  
	     // else if(event.getPointerId(i)==this.fireID)
	     // else if((endPoint[i].x>this.mapLevelOne.getHalfWidth())&&(endPoint[i].y<930))
	     if(endPoint[i].x<this.mapLevelOne.getHalfWidth())
	      {
//	    	  System.out.println("开火放开-->"+endPoint.x); 
//	    	  System.out.println("开火放开ID-->"+event.getPointerId(i));
	    	//  Log.e("开火放开ID-->",event.getPointerId(i)+"");
	    	  this.fireTouchEnded=System.currentTimeMillis();
	    	  
	    	  if((this.fireTouchEnded-this.fireTouchBegan)<1000)
	    	  {
	    		  CCSprite tempSprite =this.bulletLevelOne.getCookBullet(this);
				  tempSprite.setVisible(true);
				  bulletLevelOne.setBulletPos(tempSprite, cookSingle.getCookSprite(),this.cookSingle.getDirection());
				  bulletLevelOne.createCookBulletAnimation(tempSprite,tempSprite.getTag()); 
	    	  }
	    	  this.fire=0;  
	    	  this.unschedule("fire");
	    	  this.fireID =10;                            //开火后将其置为10（即无触碰状态）
	    	  return super.ccTouchesEnded(event);
	      }
	      
		}
		}
		
		
		return super.ccTouchesEnded(event);
	}
	
	
	
	/*
	 * 计时器调用fire函数产生子弹
	 */
	

	public void fire(float delta){
		if(this.fire==1){	
			CCSprite tempSprite =this.bulletLevelOne.getCookBullet(this);
			tempSprite.setVisible(true);
			bulletLevelOne.setBulletPos(tempSprite, cookSingle.getCookSprite(),this.cookSingle.getDirection());
			bulletLevelOne.createCookBulletAnimation(tempSprite,tempSprite.getTag());
			//this.bulletLevelOne .createCookBulletAnimation(tempSprite,this.cookBulletDirection);	
		}	
	}
	
	/*
	 * 计时器调用createDog函数产生dog
	 */
	//private int i=0;
	public void createDog(float delta){
		if(((this.dogList.getLevelOneDog1().size()+this.dogList.getLevelOneDog2().size())<=0)
		    ||(this.dogList.getRunDog1().size()+this.dogList.getRunDog2().size()>=GameLayer.allRunDog[level]))
		{
			return;
		}
		else 
			{
			//  born.setVisible(true);
			if(this.aboutBorn ==true){
			  CCAnimation animation = CCAnimation.animation("walk");
			    for( int i=0;i<8;i++)
						animation.addFrame(String.format("bornAnimation/frame%d.png",i+1));
			   // animation.setDelayPerUnit(0.2f);
			CCAnimate action = CCAnimate.action(0.8f,animation, false);
			CCSequence sequence=CCSequence.actions(action,CCCallFuncND.action(this,"removeBorn"));
			this.canBorn=false;
			born.setPosition(GameLayer.bornPoint[level][this.thisBorn]);
			born.runAction(sequence);
			this.aboutBorn=false;
			}
			//this.removeChild( , cleanup);
			if(this.canBorn ==true){
			Dog dog=this.dogList.getNewDog(this);
			dog.getDogSprite().setVisible(true);
			//Log.e("正在跑的狗",(this.dogList.getRunDog1().size()+this.dogList.getRunDog2().size())+"");
			//i++;
			
			/*int random=(int)(Math.random()*100%4);
			while(random==this.beforeRandomBornPos)
			{
				random=(int)(Math.random()*100%4);
			}*/
			dog.getDogSprite().setPosition(GameLayer.bornPoint[level][this.thisBorn]);
			//Log.e("关卡",level+"");
			//Log.e("生成狗",i+"");
			//Log.e("位置",GameLayer.bornPoint[level][random]+"");
			//dog.getDogSprite().setVisible(true);
			this.beforeRandomBornPos=thisBorn;
			if(thisBorn==2)
				thisBorn=0;
			else thisBorn++;
			int random1=(int)(Math.random()*100%4);
			dog.setDirection(random1);
			//this.schedule("moveDog",0.3f);
			//this.schedule("dogFire",1f);
			this.aboutBorn=true;
			}
			}
			
	}

/*public void moveDog(float delta){
		
		for(int i=0;i<this.dogList.getRunDog1().size();i++){
			//touchDog(this.dogList.getRunDog1().get(i));
			int random=(int)(Math.random()*100%4);
			this.dogList.getRunDog1().get(i).setDirection(random);
			boolean isFirstTime=true;
			boolean isCorrectDirection=false;
			while(isCorrectDirection==false)
			{
			//Log.e("循环1",i+"");
			switch(this.dogList.getRunDog1().get(i).getDirection())
			{
			case 0:if(isFirstTime)
				{if((this.dogList.getRunDog1().get(i).calRow()+1<9)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume(),this.dogList.getRunDog1().get(i).calRow()+1)<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume(),this.dogList.getRunDog1().get(i).calRow()+1)>4))
					&&(!this.assistant.isNextPosDog(0,i, this.dogList)))
						isCorrectDirection=true;
				    else	this.dogList.getRunDog1().get(i).setDirection(1);
				}
			else{
				if((this.dogList.getRunDog1().get(i).calRow()+1<9)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume(),this.dogList.getRunDog1().get(i).calRow()+1)<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume(),this.dogList.getRunDog1().get(i).calRow()+1)>4)))
						isCorrectDirection=true;
				    
			else	this.dogList.getRunDog1().get(i).setDirection(1);
				}
		       
			       break;
			case 1:if(isFirstTime)
				{
				if(((this.dogList.getRunDog1().get(i).calColume()+1<15)
					&&(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume()+1,this.dogList.getRunDog1().get(i).calRow())<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume()+1,this.dogList.getRunDog1().get(i).calRow())>4))
					&&(!this.assistant.isNextPosDog(0,i, this.dogList)))
						isCorrectDirection=true;
			else 
            	this.dogList.getRunDog1().get(i).setDirection(2);
				}
			else {
				if(((this.dogList.getRunDog1().get(i).calColume()+1<15)
						&&(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume()+1,this.dogList.getRunDog1().get(i).calRow())<1)
						||(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume()+1,this.dogList.getRunDog1().get(i).calRow())>4)))
							isCorrectDirection=true;
				else 
	            	this.dogList.getRunDog1().get(i).setDirection(2);
					}
       
				   break;
			case 2:if(isFirstTime)
				{if((this.dogList.getRunDog1().get(i).calRow()-1>=0)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume(),this.dogList.getRunDog1().get(i).calRow()-1)<1)
				    ||(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume(),this.dogList.getRunDog1().get(i).calRow()-1)>4))
				    &&(!this.assistant.isNextPosDog(0,i, this.dogList)))
						isCorrectDirection=true;
			else
            	this.dogList.getRunDog1().get(i).setDirection(3);
				}
			else 
			{
				if((this.dogList.getRunDog1().get(i).calRow()-1>=0)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume(),this.dogList.getRunDog1().get(i).calRow()-1)<1)
						    ||(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume(),this.dogList.getRunDog1().get(i).calRow()-1)>4)))
								isCorrectDirection=true;
					else
		            	this.dogList.getRunDog1().get(i).setDirection(3);
						}
       
				   break;
			case 3:if(isFirstTime)
				{
				if((this.dogList.getRunDog1().get(i).calColume()-1>=0)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume()-1,this.dogList.getRunDog1().get(i).calRow())<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume()-1,this.dogList.getRunDog1().get(i).calRow())>4))
					&&(!this.assistant.isNextPosDog(0,i, this.dogList)))
						isCorrectDirection=true;
			        else	this.dogList.getRunDog1().get(i).setDirection(0);
				}
			else
			{
				if((this.dogList.getRunDog1().get(i).calColume()-1>=0)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume()-1,this.dogList.getRunDog1().get(i).calRow())<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume()-1,this.dogList.getRunDog1().get(i).calRow())>4)))
						isCorrectDirection=true;
			        else	this.dogList.getRunDog1().get(i).setDirection(0);
				}
       
				   break;
			default:break;
			}
			isFirstTime=false;
			if(this.assistant.isNextPosDog(0,i, this.dogList))
				Log.e("狗1是否碰撞",this.assistant.isNextPosDog(0,i, this.dogList)+""); 
			Log.e("狗1是否碰撞",this.dogList.getRunDog1().get(i).getDirection()+"");
			
			}
			
			this.dogList.getRunDog1().get(i).dogMoveAnimation(this);
			
			
		}
		
		
		
		for(int i=0;i<this.dogList.getRunDog2().size();i++){
		//	touchDog(this.dogList.getRunDog2().get(i));
			int random1=(int)(Math.random()*100%4);
			this.dogList.getRunDog2().get(i).setDirection(random1);
			boolean isCorrectDirection1=false;
			boolean isFirstTime1=true;
			while(isCorrectDirection1==false)
			{
			//Log.e("循环2",i+"");
			int random=(int)(Math.random()*100%4);
			this.dogList.getRunDog2().get(i).setDirection(random);
			switch(this.dogList.getRunDog2().get(i).getDirection())
			{
			case 0:if(isFirstTime1)
				{
				if((this.dogList.getRunDog2().get(i).calRow()+1<9)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()+1)<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()+1)>4))
					&&(!this.assistant.isNextPosDog(1,i, this.dogList)))
						isCorrectDirection1=true;
			       else this.dogList.getRunDog2().get(i).setDirection(3);
				}
			else
			{
				if((this.dogList.getRunDog2().get(i).calRow()+1<9)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()+1)<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()+1)>4)))
						isCorrectDirection1=true;
			       else this.dogList.getRunDog2().get(i).setDirection(3);
				}
			       
			       break;
			case 1:if(isFirstTime1)
				{if((this.dogList.getRunDog2().get(i).calColume()+1<15)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume()+1,this.dogList.getRunDog2().get(i).calRow())<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume()+1,this.dogList.getRunDog2().get(i).calRow())>4))
					&&(!this.assistant.isNextPosDog(1,i, this.dogList)))
						isCorrectDirection1=true;
			       else this.dogList.getRunDog2().get(i).setDirection(0);
				}
			else
			{
				if((this.dogList.getRunDog2().get(i).calRow()+1<9)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()+1)<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()+1)>4)))
						isCorrectDirection1=true;
			       else this.dogList.getRunDog2().get(i).setDirection(3);
				}
				   break;
			case 2:if(isFirstTime1)
				{if((this.dogList.getRunDog2().get(i).calRow()-1>=0)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()-1)<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()-1)>4))
					&&(!this.assistant.isNextPosDog(1,i, this.dogList)))
						isCorrectDirection1=true;
					else this.dogList.getRunDog2().get(i).setDirection(1);
				}
			else{
				if((this.dogList.getRunDog2().get(i).calRow()+1<9)
						&&((mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()+1)<1)
						||(mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()+1)>4)))
							isCorrectDirection1=true;
				       else this.dogList.getRunDog2().get(i).setDirection(3);
					}
	       
				   break;
			case 3:if(isFirstTime1)
				{if((this.dogList.getRunDog2().get(i).calColume()-1>=0)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume()-1,this.dogList.getRunDog2().get(i).calRow())<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume()-1,this.dogList.getRunDog2().get(i).calRow())>4))
					&&(!this.assistant.isNextPosDog(1,i, this.dogList)))
						isCorrectDirection1=true;
			 else
	            	this.dogList.getRunDog2().get(i).setDirection(2);
				}
			else {
				if((this.dogList.getRunDog2().get(i).calRow()+1<9)
						&&((mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()+1)<1)
						||(mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()+1)>4)))
							isCorrectDirection1=true;
				       else this.dogList.getRunDog2().get(i).setDirection(3);
					}
	       	   break;
			default:break;
			}
			
			if(this.assistant.isNextPosDog(1,i, this.dogList))
			{
				Log.e("狗2是否碰撞",this.assistant.isNextPosDog(1,i, this.dogList)+"");
				Log.e("狗2是否碰撞",this.dogList.getRunDog2().get(i).getDirection()+"");
			}
			
			isFirstTime1=false;
			}
			
			this.dogList.getRunDog2().get(i).dogMoveAnimation(this);
		    
			
		}
	}*/
	
	
public void moveDog(float delta){
	if(this.dogList.getRunDog1().size()>0)	
	{
		for(int i=0;i<this.dogList.getRunDog1().size();i++){
			//touchDog(this.dogList.getRunDog1().get(i));
			if(this.dogList.getRunDog1().get(i).isGetRandomDirection())
			{
			int random=(int)(Math.random()*100%4);
			//this.dogList.getRunDog1().get(i).setDirection(random);
			/*if(this.assistant.isNextPosDog(0,i, this.dogList))
			{
				if(random==3)
					this.dogList.getRunDog1().get(i).setDirection(0);
				else this.dogList.getRunDog1().get(i).setDirection(random+1);
			}
			else*/
			 this.dogList.getRunDog1().get(i).setDirection(random);
			}
			boolean isCorrectDirection=false;
			while(isCorrectDirection==false)
			{
			//Log.e("循环1",i+"");
			switch(this.dogList.getRunDog1().get(i).getDirection())
			{
			case 0:if((this.dogList.getRunDog1().get(i).calRow()+1<9)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume(),this.dogList.getRunDog1().get(i).calRow()+1)<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume(),this.dogList.getRunDog1().get(i).calRow()+1)>6)))
						isCorrectDirection=true;
				    else	this.dogList.getRunDog1().get(i).setDirection(1);
			
			       break;
			case 1:
				if(((this.dogList.getRunDog1().get(i).calColume()+1<15)
					&&(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume()+1,this.dogList.getRunDog1().get(i).calRow())<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume()+1,this.dogList.getRunDog1().get(i).calRow())>6)))
						isCorrectDirection=true;
			else 
            	this.dogList.getRunDog1().get(i).setDirection(2);
				
				   break;
			case 2:if((this.dogList.getRunDog1().get(i).calRow()-1>=0)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume(),this.dogList.getRunDog1().get(i).calRow()-1)<1)
				    ||(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume(),this.dogList.getRunDog1().get(i).calRow()-1)>6)))
						isCorrectDirection=true;
			else
            	this.dogList.getRunDog1().get(i).setDirection(3);
				
       
				   break;
			case 3:
				if((this.dogList.getRunDog1().get(i).calColume()-1>=0)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume()-1,this.dogList.getRunDog1().get(i).calRow())<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog1().get(i).calColume()-1,this.dogList.getRunDog1().get(i).calRow())>6)))
						isCorrectDirection=true;
			        else	this.dogList.getRunDog1().get(i).setDirection(0);
				
				   break;
			default:break;
			}
			
			/*if(this.assistant.isNextPosDog(0,i, this.dogList))
				Log.e("狗1是否碰撞",this.assistant.isNextPosDog(0,i, this.dogList)+""); 
			Log.e("狗1是否碰撞",this.dogList.getRunDog1().get(i).getDirection()+"");*/
			
			}
			
			this.dogList.getRunDog1().get(i).dogMoveAnimation(this);
			this.dogList.getRunDog1().get(i).setGetRandomDirection(true);
			
		}
		
	}	
		
	if(this.dogList.getRunDog2().size()>0)
	{
		for(int i=0;i<this.dogList.getRunDog2().size();i++){
		//	touchDog(this.dogList.getRunDog2().get(i));
			if(this.dogList.getRunDog2().get(i).isGetRandomDirection())
			{
			int random1=(int)(Math.random()*100%4);
			//this.dogList.getRunDog2().get(i).setDirection(random1);
			/*if(this.assistant.isNextPosDog(1,i, this.dogList))
			{
				if(random1==0)
					this.dogList.getRunDog2().get(i).setDirection(3);
				else this.dogList.getRunDog2().get(i).setDirection(random1-1);
			}
			else */
			this.dogList.getRunDog2().get(i).setDirection(random1);
			}
			boolean isCorrectDirection1=false;
			
			while(isCorrectDirection1==false)
			{
			//Log.e("循环2",i+"");
			//int random=(int)(Math.random()*100%4);
			//this.dogList.getRunDog2().get(i).setDirection(random);
			switch(this.dogList.getRunDog2().get(i).getDirection())
			{
			case 0:
				if((this.dogList.getRunDog2().get(i).calRow()+1<9)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()+1)<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()+1)>6)))
						isCorrectDirection1=true;
			       else this.dogList.getRunDog2().get(i).setDirection(3);
				
			       
			       break;
			case 1:if((this.dogList.getRunDog2().get(i).calColume()+1<15)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume()+1,this.dogList.getRunDog2().get(i).calRow())<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume()+1,this.dogList.getRunDog2().get(i).calRow())>6)))
						isCorrectDirection1=true;
			       else this.dogList.getRunDog2().get(i).setDirection(0);
				
				   break;
			case 2:if((this.dogList.getRunDog2().get(i).calRow()-1>=0)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()-1)<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume(),this.dogList.getRunDog2().get(i).calRow()-1)>6)))
						isCorrectDirection1=true;
					else this.dogList.getRunDog2().get(i).setDirection(1);
				break;
			case 3:if((this.dogList.getRunDog2().get(i).calColume()-1>=0)
					&&((mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume()-1,this.dogList.getRunDog2().get(i).calRow())<1)
					||(mapLevelOne.getMapContent(this.dogList.getRunDog2().get(i).calColume()-1,this.dogList.getRunDog2().get(i).calRow())>6)))
						isCorrectDirection1=true;
			 else
	            	this.dogList.getRunDog2().get(i).setDirection(2);
			
	       	   break;
			default:break;
			}
		/*	
			if(this.assistant.isNextPosDog(1,i, this.dogList))
			{
				Log.e("狗2是否碰撞",this.assistant.isNextPosDog(1,i, this.dogList)+"");
				Log.e("狗2是否碰撞",this.dogList.getRunDog2().get(i).getDirection()+"");
			}
			
			*/
			
			}
			
			this.dogList.getRunDog2().get(i).dogMoveAnimation(this);
			this.dogList.getRunDog2().get(i).setGetRandomDirection(true); 
			
		}
	}
	}
	
	
	public void dogFire(float delta){
		//for(int i=0;i<this.dogList.getRunDog1().size();i++){
	if(this.newDog ==0)
		{
		CCSprite tempSprite =this.bulletLevelOne.getBoneBullet(this);
		tempSprite.setVisible(true);
		bulletLevelOne.setBulletPos(tempSprite,this.dogList.getRunDog1().get(this.dogList.getRunDog1().size()-1).getDogSprite(),this.dogList.getRunDog1().get(this.dogList.getRunDog1().size()-1).getDirection());
		bulletLevelOne.createBoneAnimation(tempSprite,tempSprite.getTag());
		
				}
		//}	
		//for(int i=0;i<this.dogList.getRunDog2().size();i++){
		if(this.newDog ==1)
		{
			CCSprite tempSprite =this.bulletLevelOne.getBoneBullet(this);
			tempSprite.setVisible(true);
			bulletLevelOne.setBulletPos(tempSprite,this.dogList.getRunDog2().get(this.dogList.getRunDog2().size()-1).getDogSprite(),this.dogList.getRunDog2().get(this.dogList.getRunDog2().size()-1).getDirection());
			bulletLevelOne.createBoneAnimation(tempSprite,tempSprite.getTag());
		}
		//	}	
	}
	
	
	/*
	 * 计时器调用此函数检查子弹是否出屏幕或是达到障碍物，或者打到精灵等
	 */
	public void checkBullet(float delta){
		if(this.bulletLevelOne.getRunBulletList().size()>0){	
			//boolean shouldRemove=false;
			for(int i=0;i<this.bulletLevelOne.getRunBulletList().size();i++)
			{
				//检查是否打到第一种狗
				if(this.dogList.getRunDog1().size()>0)
				{	
					for(int j=0;j<this.dogList.getRunDog1().size();j++)	
					{
						if(this.assistant.isTouch(this.bulletLevelOne.getRunBulletList().get(i).getBoundingBox(), this.dogList.getRunDog1().get(j).getDogSprite().getBoundingBox()))
						{
							this.cookSingle.setMoney(this.cookSingle.getMoney()+1);
							//this.bulletLevelOne.deleteCookBullet(i, this);
							//this.dogList.deleteDog(0, j, this);
							/*int random=(int)(Math.random()*5);
							CCSprite sprite =CCSprite.sprite(this.giftSheet,CGRect.make(random*120,0,120,120));
							sprite.setTag(random+7);
							int random2=(int)(Math.random()*135);
							//Log.e("beHit1",random2+"");
							while(this.mapLevelOne.getMapContent(random2%15,(int)random2/15)!=0)
							{
								random2=(int)(Math.random()*135);
								Log.e("beHit1",random2+"");
							}
							sprite.setPosition(120*(random2%15)+60,120*((int)(random2/15))+60);
							this.mapLevelOne.setMapContent(random2%15,(int)(random2/15),sprite.getTag());
							this.giftSheet.addChild(sprite);
							this.gift.add(sprite);	*/
							this.effect.getDeleteBullet().setPosition(this.bulletLevelOne.getRunBulletList().get(i).getPosition());
							this.effect.getDeleteBullet().setVisible(true);
							this.effect.createDeleteAnimation();
							this.bulletLevelOne.deleteCookBullet(i, this);
							
							
							this.dogList.deleteDog(0, j, this);
							return;
						}
					}
				}
				
			 //检查是否打到第二种狗
				if(this.dogList.getRunDog2().size()>0)
				{	
					for(int j=0;j<this.dogList.getRunDog2().size();j++)	
					{
						if(this.assistant.isTouch(this.bulletLevelOne.getRunBulletList().get(i).getBoundingBox(), this.dogList.getRunDog2().get(j).getDogSprite().getBoundingBox()))
						{ 
							this.effect.getDeleteBullet().setPosition(this.bulletLevelOne.getRunBulletList().get(i).getPosition());
							this.effect.getDeleteBullet().setVisible(true);
							this.effect.createDeleteAnimation();
							this.bulletLevelOne.deleteCookBullet(i, this);
							this.dogList.getRunDog2().get(j).setBeHit(this.dogList.getRunDog2().get(j).getBeHit()+1);
							if(this.dogList.getRunDog2().get(j).getBeHit()>=2)
							{
							this.cookSingle.setMoney(this.cookSingle.getMoney()+2);
							//this.dogList.deleteDog(1, j, this);
							int random3=(int)(Math.random()*5);
							CCSprite sprite1 =CCSprite.sprite(this.giftSheet,CGRect.make(random3*120,0,120,120));
							sprite1.setTag(random3+7);
							int random4=(int)(Math.random()*135);
							//Log.e("beHit2",random4+"");
							while(this.mapLevelOne.getMapContent(random4%15,(int)(random4/15))!=0)
							{
								random4=(int)(Math.random()*135);
							}
							sprite1.setPosition(120*(random4%15)+60,120*((int)(random4/15))+60);
							this.mapLevelOne.setMapContent(random4%15,(int)(random4/15),sprite1.getTag());
							this.giftSheet.addChild(sprite1);
							this.gift.add(sprite1);	
							this.dogList.deleteDog(1, j, this);
							return;
							}
						}
					}
				}
			
				
			//检查是否打到障碍物
			if(this.mapObstacle.size()!=0)
			{
				for(int m=0;m<this.mapObstacle.size();m++)
					{
						if(this.assistant.isTouch(this.bulletLevelOne.getRunBulletList().get(i).getBoundingBox(), this.mapObstacle.get(m).getBoundingBox()))
						{   
							this.effect.getDeleteBullet().setPosition(this.bulletLevelOne.getRunBulletList().get(i).getPosition());
							this.effect.getDeleteBullet().setVisible(true);
							this.effect.createDeleteAnimation();
							this.bulletLevelOne.deleteCookBullet(i, this);
							//this.dogList.getRunDog2().get(j).setBeHit(this.dogList.getRunDog2().get(j).getBeHit()+1);
							if(this.mapObstacle.get(m).getTag()==4)
								{
								//CCSprite sprite =CCSprite.sprite("obstacle/obstacleStoneHalf.png");
								CCSprite sprite1=this.mapObstacle.get(m);
								//this.obstacleLevelOneSheet.removeChild(sprite1, true);
								//this.addChild(sprite1);
								CCSprite sprite=CCSprite.sprite(obstacleLevelOneSheet,CGRect.make(360,0,120,120));
								sprite.setTag(5);
								sprite.setPosition(sprite1.getPosition());
								sprite1.setVisible(false);
								sprite.setVisible(true);
								this.mapObstacle.remove(sprite1);
								this.obstacleLevelOneSheet.removeChild(sprite1, true);
								this.removeChild(sprite1,true);
								this.mapObstacle.add(sprite);
								this.obstacleLevelOneSheet.addChild(sprite);
								}
							else
							{
								CCSprite sprite=this.mapObstacle.get(m);
								sprite.setVisible(false);
								int colume=(int)sprite.getPosition().x/120;
								int row=(int)sprite.getPosition().y/120;
								this.mapLevelOne.setMapContent(colume, row, 0);
								this.mapObstacle.remove(sprite);
								this.obstacleLevelOneSheet.removeChild(sprite, true);
								this.removeChild(sprite,true);
								
								return;
							}
						}
					}
			}
				
				
			//检查是否打到边境
				
			if((this.bulletLevelOne.getRunBulletList().get(i).getPosition().x>=(1800-50-Common.getSpriteWidth(this.bulletLevelOne.getRunBulletList().get(i))))
				||(this.bulletLevelOne.getRunBulletList().get(i).getPosition().x<=Common.getSpriteWidth(this.bulletLevelOne.getRunBulletList().get(i))+70)
			   ||(this.bulletLevelOne.getRunBulletList().get(i).getPosition().y>=(1080-60-Common.getSpriteHeight(this.bulletLevelOne.getRunBulletList().get(i))))
			   ||(this.bulletLevelOne.getRunBulletList().get(i).getPosition().y<=Common.getSpriteHeight(this.bulletLevelOne.getRunBulletList().get(i))+60))
			{
				this.effect.getDeleteBullet().setPosition(this.bulletLevelOne.getRunBulletList().get(i).getPosition());
				this.effect.getDeleteBullet().setVisible(true);
				this.effect.createDeleteAnimation();
				this.bulletLevelOne.deleteCookBullet(i, this);
			}
				
				
				
				
				
				
				
				
				
			}
		
	
		}
	
		//检查骨头
		if(this.bulletLevelOne.getRunBoneList().size()>0){
			
		for(int i=0;i<this.bulletLevelOne.getRunBoneList().size();i++){
			
			//检查是否打到障碍物
			if(this.mapObstacle.size()!=0)
			{
				for(int m=0;m<this.mapObstacle.size();m++)
					{
						if(this.assistant.isTouch(this.bulletLevelOne.getRunBoneList().get(i).getBoundingBox(), this.mapObstacle.get(m).getBoundingBox()))
						{   
							this.effect.getDeleteBone().setPosition(this.bulletLevelOne.getRunBoneList().get(i).getPosition());
							this.effect.getDeleteBone().setVisible(true);
							this.effect.createDeleteBoneAnimation();
							this.bulletLevelOne.deleteBone(i, this);
							//this.dogList.getRunDog2().get(j).setBeHit(this.dogList.getRunDog2().get(j).getBeHit()+1);
							if(this.mapObstacle.get(m).getTag()==4)
								{
								//CCSprite sprite =CCSprite.sprite("obstacle/obstacleStoneHalf.png");
								CCSprite sprite1=this.mapObstacle.get(m);
								//this.obstacleLevelOneSheet.removeChild(sprite1, true);
								//this.addChild(sprite1);
								CCSprite sprite=CCSprite.sprite(obstacleLevelOneSheet,CGRect.make(360,0,120,120));
								sprite.setTag(5);
								sprite.setPosition(sprite1.getPosition());
								sprite1.setVisible(false);
								sprite.setVisible(true);
								this.mapObstacle.remove(sprite1);
								this.obstacleLevelOneSheet.removeChild(sprite1, true);
								this.removeChild(sprite1,true);
								this.mapObstacle.add(sprite);
								this.obstacleLevelOneSheet.addChild(sprite);
								}
							else
							{
								CCSprite sprite=this.mapObstacle.get(m);
								sprite.setVisible(false);
								int colume=(int)sprite.getPosition().x/120;
								int row=(int)sprite.getPosition().y/120;
								this.mapLevelOne.setMapContent(colume, row, 0);
								this.mapObstacle.remove(sprite);
								this.obstacleLevelOneSheet.removeChild(sprite, true);
								this.removeChild(sprite,true);
								
								return;
							}
						}
					}
			}
				
				
			//检查是否打到边境
				
			if((this.bulletLevelOne.getRunBoneList().get(i).getPosition().x>=(1800-50-Common.getSpriteWidth(this.bulletLevelOne.getRunBoneList().get(i))))
				||(this.bulletLevelOne.getRunBoneList().get(i).getPosition().x<=Common.getSpriteWidth(this.bulletLevelOne.getRunBoneList().get(i))+70)
			   ||(this.bulletLevelOne.getRunBoneList().get(i).getPosition().y>=(1080-60-Common.getSpriteHeight(this.bulletLevelOne.getRunBoneList().get(i))))
			   ||(this.bulletLevelOne.getRunBoneList().get(i).getPosition().y<=Common.getSpriteHeight(this.bulletLevelOne.getRunBoneList().get(i))+60))
			{
				this.effect.getDeleteBone().setPosition(this.bulletLevelOne.getRunBoneList().get(i).getPosition());
				this.effect.getDeleteBone().setVisible(true);
				this.effect.createDeleteBoneAnimation();
				this.bulletLevelOne.deleteBone(i, this);
			}
				
				
		//判断是否打到厨师
			if(this.assistant.isTouch(this.bulletLevelOne.getRunBoneList().get(i).getBoundingBox(), this.cookSingle.getCookSprite().getBoundingBox()))
		     {
				this.effect.getDeleteBone().setPosition(this.bulletLevelOne.getRunBoneList().get(i).getPosition());
				this.effect.getDeleteBone().setVisible(true);
				this.effect.createDeleteBoneAnimation();
				this.bulletLevelOne.deleteBone(i, this);
				this.cookSingle.setBeHit(this.cookSingle.getBeHit()+1);
			 }	
		}		
		}	
		
	}
	
	
	
	
	
	public void checkTouch(float delta){
		
	//检查狗与狗之间的碰撞
	if(this.dogList.getRunDog1().size()+this.dogList.getRunDog2().size()>1){
			if(this.dogList.getRunDog1().size()>0)
			{
				for(int i=0;i<this.dogList.getRunDog1().size();i++)
				{ 
					if(this.assistant.isTouchDog(0, i, this.dogList))
					{
					    this.dogList.getRunDog1().get(i).setGetRandomDirection(false);
						int direction=this.dogList.getRunDog1().get(i).getDirection();
						if(direction!=3)
						this.dogList.getRunDog1().get(i).setDirection(direction+1);
						else this.dogList.getRunDog1().get(i).setDirection(0);
						//this.dogList.deleteDog(0, i, this);

					}
				}			
		}
		
			
	if(this.dogList.getRunDog2().size()>0)
	{
		for(int i=0;i<this.dogList.getRunDog2().size();i++)
		{ 
			if(this.assistant.isTouchDog(1, i, this.dogList))
			{
				this.dogList.getRunDog2().get(i).setGetRandomDirection(false);
				int direction=this.dogList.getRunDog2().get(i).getDirection();
				if(direction!=0)
				this.dogList.getRunDog2().get(i).setDirection(direction-1);
				else this.dogList.getRunDog2().get(i).setDirection(3);
				//this.dogList.deleteDog(1, i, this);

			}
		}		
		
	}
		
		
	
	}
	
	}
	
	
	
	//处理cook状态以及胜利条件检测
	
	public void checkCookCondition(float delta)
	{
		//检测是否捡到gift
		if(this.gift.size()>0)
		{
		for(int i=0;i<this.gift.size();i++){
			CCSprite sprite=this.gift.get(i);
			if(this.assistant.isTouch(this.cookSingle.getCookSprite().getBoundingBox(),sprite.getBoundingBox()))
			{
				switch(sprite.getTag())
				{
				case 7:this.cookSingle.setBeHit(this.cookSingle.getBeHit()-1);
//				       this.
				       //this.cookSingle.setGiftSituation(1);
				       break;
				case 8:this.cookSingle.setGiftSituation(2);
				       break;
				case 9:this.cookSingle.setGiftSituation(3);
			            break;
				case 10:this.cookSingle.setGiftSituation(4);
			            break;
				case 11://this.cookSingle.setGiftSituation(5);
				        this.cookSingle.setMoney(this.cookSingle.getMoney()+1);
			            break;
			    default:break;
				}
			}
		}
		}
		
		
		
		
		
		
		
		
		
		
		
	}
	
	
	
	
	
	
	/*
	 * 狗与狗之间的碰撞检测
 */
//public void dogTouch(){
	/*public void dogTouch(float delta){
		if((this.dogList.getRunDog1().size()+this.dogList.getRunDog2().size())>1)
		{
		Dog[] tempDog=new Dog[this.allRunDog[this.level]];
		if(this.dogList.getRunDog1().size()>0){
		for(int i=0;i<this.dogList.getRunDog1().size();i++)
			tempDog[i]=this.dogList.getRunDog1().get(i);
		}
		if(this.dogList.getRunDog2().size()>0){
		for(int j=0;j<this.dogList.getRunDog2().size();j++)
			tempDog[j+this.dogList.getRunDog2().size()]=this.dogList.getRunDog2().get(j);
		}
	    for(int m=0;m<this.dogList.getRunDog1().size()+this.dogList.getRunDog2().size()-1;m++){
	    	for(int n=m+1;n<this.dogList.getRunDog1().size()+this.dogList.getRunDog2().size();n++){
	    	    CGRect rectA=tempDog[m].getDogSprite().getBoundingBox();
	    	    CGRect rectB=tempDog[n].getDogSprite().getBoundingBox();
	    		if(this.assistant.isTouch(rectA, rectB))
	    		{
	    			Log.e("重叠",m+"");
	    			tempDog[m].setBlock(true);
	    			tempDog[n].setBlock(true);
	    			
	    		}
	    		else
	    		{
	    			tempDog[m].setBlock(false);
	    			tempDog[n].setBlock(false);
	    		}
	    	}
	    }
		}
		
		
		
		
	}
	*/
	
	public void touchDog(Dog dog){
		if((this.dogList.getRunDog1().size()+this.dogList.getRunDog2().size())>1)
		{
		Dog[] tempDog=new Dog[this.allRunDog[this.level]];
		if(this.dogList.getRunDog1().size()>0){
		for(int i=0;i<this.dogList.getRunDog1().size();i++)
			tempDog[i]=this.dogList.getRunDog1().get(i);
		}
		if(this.dogList.getRunDog2().size()>0){
		for(int j=0;j<this.dogList.getRunDog2().size();j++)
			tempDog[j+this.dogList.getRunDog2().size()]=this.dogList.getRunDog2().get(j);
		}
		
		for(int m=0;m<this.dogList.getRunDog1().size()+this.dogList.getRunDog2().size();m++){
			 CGRect rectA=tempDog[m].getDogSprite().getBoundingBox();
	    	 CGRect rectB=dog.getDogSprite().getBoundingBox();
	    	  if(this.assistant.isTouch(rectA, rectB)==true)
	    	  {
	    		  dog.setBlock(true);
	    	  }
	    	  else dog.setBlock(false);
		}
		
		
		
		
		
		}
	}
	
	
	
	
	
	
	public void removeBorn(Object Sender){
		//this.removeChild(this.born, true);
		this.canBorn=true;
	}
}
