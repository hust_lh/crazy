package com.crazy.service;

import org.cocos2d.actions.base.CCRepeatForever;
import org.cocos2d.actions.interval.CCScaleTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItem;
import org.cocos2d.menus.CCMenuItemImage;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteSheet;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import android.view.MotionEvent;

import com.crazy.ui.Common;

public class MapEdit extends CCLayer {
	
	//存储用户编辑后的地图的数组
	private int mapArray[][] =  new int[9][15];	/**存放障碍物的枚举值。
												0：没有障碍物；
												1：不能放置障碍物和不能通行的地方，比如荷池，树木
												2：障碍物1；
												3：障碍物2；
												4：障碍物3*/
	private final int LENGTH = 30;	//障碍物数组长度
	//记录已添加的障碍物数组
	private CCSprite obstacle1Array[] = new CCSprite[LENGTH];
	private CCSprite obstacle2Array[] = new CCSprite[LENGTH];
	private CCSprite obstacle3Array[] = new CCSprite[LENGTH];
	private int row,column;		//触控点所在的行列
	/**一些标识符**/
	private boolean isTouch = false;	//触控判断，false允许出现待选障碍物精灵，true不允许出现
	private boolean isSelect = false;	//障碍物精灵选择判断，false允许选择，true不允许选择
	private boolean isAlert = false;	//判断是否弹出了对话框，如果弹出了，那就不允许点击屏幕自定义地图
	
	//障碍物存放数组索引
	private int obstacle1Num=0;
	private int obstacle2Num=0;
	private int obstacle3Num=0;
	
	//地图编辑界面的背景
	private CCSprite mapEditBg = CCSprite.sprite("editMap/map1.png");
	//标识触控区域的精灵对象
	private CCSpriteSheet obstacleSheet = CCSpriteSheet.spriteSheet("editMap/obstacleOneSheet.png");
	private CCSprite touch = CCSprite.sprite("editMap/touch1.png");
	/**障碍物精灵（注意！！！这里障碍物obstacle1、obstacle2、obstacle3的贴图顺序应当跟obstacleSheet所对应的图片中的障碍物顺序相同，
	否则在编辑地图时会导致点击A出现B的情况，如若有疑问我们交流一下）*/
	private CCSprite obstacle1 = CCSprite.sprite("editMap/obstacleBox.png");
	private CCSprite obstacle2 = CCSprite.sprite("editMap/obstacleStool.png");
	private CCSprite obstacle3 = CCSprite.sprite("editMap/obstacleStone.png");
	//障碍物精灵，用于提示用户此类障碍物已没有了，不能再添加了
	private CCSprite obstacle1No = CCSprite.sprite("editMap/obstacleBox_1.png");
	private CCSprite obstacle2No = CCSprite.sprite("editMap/obstacleStool_1.png");
	private CCSprite obstacle3No = CCSprite.sprite("editMap/obstacleStone_1.png");
	//按钮
	private CCSprite overBtn = CCSprite.sprite("editMap/over.png");//完成按钮
	private CCSprite alertDialog = CCSprite.sprite("editMap/alert/alertBg.png");//弹出框背景
	private CCSprite ok = CCSprite.sprite("editMap/alert/ok_2.png");//弹出框上的确定按钮，用于后面布局时计算位置
	private CCSprite cancel = CCSprite.sprite("editMap/alert/cancel_2.png");//弹出框上的取消按钮，用于后面布局时计算位置
	private CCMenuItem okItem = CCMenuItemImage.item("editMap/alert/ok_2.png", "editMap/alert/ok_1.png",this,"ok");
	private CCMenuItem cancelItem = CCMenuItemImage.item("editMap/alert/cancel_2.png", "editMap/alert/cancel_1.png",this,"cancel");
	//动画控制（每一个精灵都对应一个动画，虽然效果都是一样的）
	private CCScaleTo scaleToLarge1 = CCScaleTo.action(0.1f, 1.1f);//放大为原来的1.1倍
	private CCScaleTo scaleToLarge2 = CCScaleTo.action(0.1f, 1.1f);
	private CCScaleTo scaleToLarge3 = CCScaleTo.action(0.1f, 1.1f);
	private CCScaleTo scaleToSmall1 = CCScaleTo.action(0.1f, 0);//缩小至0
	private CCScaleTo scaleToSmall2 = CCScaleTo.action(0.1f, 0);
	private CCScaleTo scaleToSmall3 = CCScaleTo.action(0.1f, 0);
	
	private CCScaleTo scaleToLarge1No = CCScaleTo.action(0.1f, 1.1f);
	private CCScaleTo scaleToSmall1No = CCScaleTo.action(0.1f, 0);
	private CCScaleTo scaleToLarge2No = CCScaleTo.action(0.1f, 1.1f);
	private CCScaleTo scaleToSmall2No = CCScaleTo.action(0.1f, 0);
	private CCScaleTo scaleToLarge3No = CCScaleTo.action(0.1f, 1.1f);
	private CCScaleTo scaleToSmall3No = CCScaleTo.action(0.1f, 0);
	
	private Common common = new Common();

	public MapEdit() {
		// TODO Auto-generated constructor stub
		this.setIsTouchEnabled(true);
		initMapEdit();
		//初始化不能添加障碍物的地图区域，包括荷池和有树的地方
		mapArray[0][2] = 1;
		mapArray[0][3] = 1;
		mapArray[0][14] = 1;
		mapArray[1][14] = 1;
		mapArray[3][0] = 1;
		mapArray[3][12] = 1;
		mapArray[3][13] = 1;
		mapArray[4][0] = 1;
		mapArray[4][11] = 1;
		mapArray[4][12] = 1;
		mapArray[7][2] = 1;
		mapArray[7][3] = 1;
		mapArray[8][2] = 1;
		mapArray[8][3] = 1;
	}
	
	//初始化，绘制背景
	public void initMapEdit(){
		
		CGPoint mapEditPoint = CGPoint.ccp(0+Common.getSpriteWidth(mapEditBg),
				0+Common.getSpriteHeight(mapEditBg));
		mapEditBg.setPosition(mapEditPoint);
		this.addChild(mapEditBg);
		
		obstacleSheet.setPosition(0, 0);
		this.addChild(obstacleSheet);
		
		CGPoint overPoint = CGPoint.ccp(1500+Common.getSpriteWidth(overBtn),
				920+Common.getSpriteHeight(overBtn));
		overBtn.setPosition(overPoint);
		this.addChild(overBtn);
		
		/**以下绘制弹出对话框**/
		CGPoint alertPoint = CGPoint.ccp(480+Common.getSpriteWidth(alertDialog), 
				300+Common.getSpriteHeight(alertDialog));
		alertDialog.setPosition(alertPoint);//暂不将alertDialog添加至layer中，待需要时再动态添加
		
		CCMenu cancelMenu = CCMenu.menu(cancelItem);
		CGPoint cancelPoint = CGPoint.ccp(225+Common.getSpriteWidth(cancel), 
				(480-471)+Common.getSpriteHeight(cancel));//这里的坐标是相对于弹出框的坐标。下同
		cancelMenu.setPosition(cancelPoint);
		alertDialog.addChild(cancelMenu);
		
		CCMenu okMenu = CCMenu.menu(okItem);
		CGPoint okPoint = CGPoint.ccp(462+Common.getSpriteWidth(ok), 
				(480-469)+Common.getSpriteHeight(ok));
		okMenu.setPosition(okPoint);
		alertDialog.addChild(okMenu);
	}
	
	//计算当前触控点是地图上的第几行
	public int calRow(CGPoint touchPoint){
		int row;
		row = (int)touchPoint.y/120;
		return row;
	}
	
	//计算当前触控点是地图上的第几列
	public int calColumn(CGPoint touchPoint){
		int column;
		column = (int)touchPoint.x/120;
		return column;
	}
	
	//绘制显示触控标识精灵对象，传入参数为当前触控点的坐标
	public void showTouch(CGPoint touchPoint){
		//获取触控点所在地图块
		row = this.calRow(touchPoint);
		column = this.calColumn(touchPoint);
		
		//计算触控区域标识精灵的坐标
		float X = 120*column + Common.getSpriteWidth(touch);
		float Y = 120*row + Common.getSpriteHeight(touch);
		
		//动画控制
		CCScaleTo large = CCScaleTo.action(0.5f, 1.1f);
		CCScaleTo small = CCScaleTo.action(0.5f, 0.9f);
		CCSequence seq = CCSequence.actions(small, large);
		CCRepeatForever forover = CCRepeatForever.action(seq);
		//显示精灵对象，以标识当前触控区域
		touch.setPosition(X, Y);
		touch.runAction(forover);
		this.addChild(touch);
	}
	
	//绘制可供选择的障碍物精灵对象，传入参数为当前触控点的坐标
	public void showObstacle(CGPoint touchPoint){
		float x = touchPoint.x;		//触控点的横坐标
		float y = touchPoint.y;		//触控点的纵坐标
		float border = 60;	//左右边界预设值
		float padding = 40;	//上下边界值和水平间距预设
		float tmpX = 60; 	//障碍物图片宽度的一半
		float obstacleX , obstacleY = 0;	//障碍物显示在地图上的横纵坐标计算基点
		float width = 2*Common.getSpriteWidth(mapEditBg);	//获取初始背景图片的宽
		float height = 2*Common.getSpriteHeight(mapEditBg);	//获取初始背景图片的高
		float obstacle1X,obstacle2X,obstacle3X;	//显示可供选择的障碍物横坐标
		
		/**
		 * 下面对当前触控点进行判断，以确定障碍物的显示位置
		 * 
		 */
		//对触控点进行横向判断,小于预设值就认为是在最边上
		if(x < (2*border + 3*tmpX)){	//触控点位于屏幕最左边
			obstacleX = border + tmpX;
			obstacle1X = obstacleX;
			obstacle2X = obstacleX + 2*tmpX + padding;
			obstacle3X = obstacleX + 4*tmpX + 2*padding;
		}else if((width-x) < (2*border + 3*tmpX)){		//触控点位于屏幕最右边
			obstacleX = width - (border + tmpX);
			obstacle1X = obstacleX - 4*tmpX - 2*padding;
			obstacle2X = obstacleX - 2*tmpX - padding;
			obstacle3X = obstacleX;
		}else{		//既不在最左边，也不在最右边
			obstacleX  = 120*column + tmpX;
			obstacle1X = obstacleX - padding - 2*tmpX;
			obstacle2X = obstacleX;
			obstacle3X = obstacleX + padding +2*tmpX;
		}
		//对触控点进行纵向判断
		if(height-y < 4*tmpX){	//位于最上面
			obstacleY = 120*(row) - tmpX - padding/2;
		}else{
			obstacleY = 120*(row+1) + tmpX + padding/2;
		}
		//对障碍物添加显示动画
		if(obstacle1Num<LENGTH){
			obstacle1.setPosition(obstacle1X, obstacleY);
			obstacle1.runAction(scaleToLarge1);
			this.addChild(obstacle1);
		}else{
			obstacle1No.setPosition(obstacle1X, obstacleY);
			obstacle1No.runAction(scaleToLarge1No);
			this.addChild(obstacle1No);
		}
		
		if(obstacle2Num<LENGTH){
			obstacle2.setPosition(obstacle2X, obstacleY);
			obstacle2.runAction(scaleToLarge2);
			this.addChild(obstacle2);
		}else{
			obstacle2No.setPosition(obstacle2X, obstacleY);
			obstacle2No.runAction(scaleToLarge2No);
			this.addChild(obstacle2No);
		}
		
		if(obstacle3Num<LENGTH){
			obstacle3.setPosition(obstacle3X, obstacleY);
			obstacle3.runAction(scaleToLarge3);
			this.addChild(obstacle3);
		}else{
			obstacle3No.setPosition(obstacle3X, obstacleY);
			obstacle3No.runAction(scaleToLarge3No);
			this.addChild(obstacle3No);
		}
	}
	
	@Override
	public boolean ccTouchesBegan(MotionEvent event) {
		// TODO Auto-generated method stub
		CGPoint touchPoint = common.getTouchPoint(event);
		boolean isClick1,isClick2,isClick3,isClick1No,isClick2No,isClick3No;
		boolean isExist = false;	//标识当前触控点是否存在已存在障碍物
		
		//判断当前是否可以自定义地图
		if(!isAlert){
			int tmpRow = this.calRow(touchPoint);	//计算当前触控点所属的行
			int tmpColumn = this.calColumn(touchPoint);		//计算当前触控点所属的列
			//判断当前触控点区域内是否已存在障碍物		
			if(!isTouch){
				if(mapArray[tmpRow][tmpColumn] == 0){
					isExist = false;	//不存在，返回false
				}else{
					isExist = true;		//存在，返回true
				}
			}
			if(!isTouch && !isExist){	//如果当前状态为允许障碍物精灵选择且触控区域不存在障碍物，那么执行相应操作
				//显示触控标识块和障碍物
				showTouch(touchPoint);
				showObstacle(touchPoint);
				isTouch = true;
			}else if(isTouch){	//如果当前已经显示了待选障碍物精灵，那么下一步执行障碍物选取操作
				if(!isSelect){
					//判断点击的是哪个障碍物
					isClick1 = common.isClick(touchPoint, obstacle1);
					isClick2 = common.isClick(touchPoint, obstacle2);
					isClick3 = common.isClick(touchPoint, obstacle3);

					isClick1No = common.isClick(touchPoint, obstacle1No);
					isClick2No = common.isClick(touchPoint, obstacle2No);
					isClick3No = common.isClick(touchPoint, obstacle3No);
					//获取绘制障碍物的位置
//					float tmpX = common.getPositionX(touch);
//					float tmpY = common.getPositionY(touch);
				
					float tmpX = 120*column + 60;
					float tmpY = 120*row + 60;
					//如果选择了第一个障碍物
					if(isClick1){
						if(obstacle1Num<LENGTH){						
							drawObstacle(obstacle1Array,obstacle1Num,tmpX,tmpY,0,0,120,120);//函数调用
							mapArray[row][column]=2;	//将mapArray中对应的位置置为相应的枚举值
							obstacle1Num++;//数组索引自增1
							isTouch = false;//将预设标识符置为false，允许点击屏幕出现待选障碍物精灵
						}
					}else if(isClick1No){//如果第一个障碍物已没有了
						isTouch = false;
					}else if(isClick2 && obstacle2Num<LENGTH){
						drawObstacle(obstacle2Array,obstacle2Num,tmpX,tmpY,120,0,120,120);
						mapArray[row][column]=3;
						obstacle2Num++;
						isTouch = false;
					}else if(isClick2No){
						isTouch = false;
					}else if(isClick3 && obstacle3Num<LENGTH){
						drawObstacle(obstacle3Array,obstacle3Num,tmpX,tmpY,240,0,120,120);
						mapArray[row][column]=4;
						obstacle3Num++;
						isTouch = false;
					}else if(isClick3No){
						isTouch = false;
					}else{
						this.removeChild(touch, false);//移除触控标识块
//						//缩小障碍物,即从layer中移除
						obstacle1.runAction(scaleToSmall1);
						obstacle2.runAction(scaleToSmall2);
						obstacle3.runAction(scaleToSmall3);
						
						//如果出现的是标识已经没有障碍物的精灵，则执行此段代码
						obstacle1No.runAction(scaleToSmall1No);
						obstacle2No.runAction(scaleToSmall2No);
						obstacle3No.runAction(scaleToSmall3No);
						
						isTouch = false;
					}
				}
			}
			//如果障碍物已存在，那么当该障碍物所在位置发生点击事件时就移除该障碍物
			if(isExist){
				for(int i=0;i<obstacle1Num;i++){
					removeObstacle(touchPoint,obstacle1Array,i,tmpRow,tmpColumn);
					/**
					 * 学霸姐思考一下这里的自减，我觉得不论是否执行自减，都会存在一些逻辑上的问题。学霸姐看到这里我们可以交流一下想法
					 */
//					obstacle1Num--;
				}
				for(int j=0;j<obstacle2Num;j++){
					removeObstacle(touchPoint,obstacle2Array,j,tmpRow,tmpColumn);
//					obstacle2Num--;
				}
				for(int k=0;k<obstacle3Num;k++){
					removeObstacle(touchPoint,obstacle3Array,k,tmpRow,tmpColumn);
//					obstacle3Num--;
				}
			}
		}
		return super.ccTouchesBegan(event);
	}
	
	//绘制选择的障碍物精灵
	public void drawObstacle(CCSprite sprite[],int index,float xPoint,float yPoint,float x,float y,float width,float height){
		//隐藏触控区域标识精灵以及障碍物选择精灵
		this.removeChild(touch, false);
		obstacle1.runAction(scaleToSmall1);
		obstacle2.runAction(scaleToSmall2);
		obstacle3.runAction(scaleToSmall3);

		obstacle1No.runAction(scaleToSmall1No);
		obstacle2No.runAction(scaleToSmall2No);
		obstacle3No.runAction(scaleToSmall3No);
		
		//绘制选择的障碍物精灵
		sprite[index] = CCSprite.sprite(obstacleSheet,CGRect.make(x, y, width, height));
		obstacleSheet.addChild(sprite[index]);
		sprite[index].runAction(scaleToLarge1);
		sprite[index].setPosition(xPoint, yPoint);
	}
	
	//取消绘制选中的障碍物精灵
	public void removeObstacle(CGPoint touchPoint,CCSprite sprite[],int i,int row,int column){
		boolean isClickObstacle;
		isClickObstacle = common.isClick(touchPoint, sprite[i]);//判断当前触控是否点击到已添加的障碍物
		if(isClickObstacle){
			sprite[i].runAction(scaleToSmall1);
			mapArray[row][column] = 0;	//取消了障碍物那就把数组中对应的位置置为0
		}
	}

	@Override
	public boolean ccTouchesEnded(MotionEvent event) {
		// TODO Auto-generated method stub
		boolean isClick;
//		int count=0;//only for test
		CGPoint touchPoint = common.getTouchPoint(event);
		isClick = common.isClick(touchPoint, overBtn);
		if(isClick && !isAlert){
			//弹出对话框
			isAlert = true;
			this.addChild(alertDialog);
			//only for test
//			for(int i=0;i<9;i++){
//				for(int j=0;j<15;j++){
//					if(mapArray[i][j] != 0){
//						count++;
//						System.out.println("mapArray["+i+"]["+j+"]="+mapArray[i][j]);
//					}
//				}
//			}
//			System.out.println("count="+count);
		}
		return super.ccTouchesEnded(event);
	}
	
	public void ok(Object sender){
		isAlert = true;
		//隐藏触控区域标识精灵以及障碍物选择精灵
		this.removeChild(touch, false);
		obstacle1.runAction(scaleToSmall1);
		obstacle2.runAction(scaleToSmall2);
		obstacle3.runAction(scaleToSmall3);	
		//隐藏对话框
		this.removeChild(alertDialog, false);
		overBtn.runAction(Common.hide);
		this.removeChild(overBtn, true);
	}
	
	public void cancel(Object sender){
		isAlert = false;
		//隐藏对话框
		this.removeChild(alertDialog, true);
	}

}
